/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : luoyu_monitor1

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2021-06-06 21:54:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for core_device
-- ----------------------------
DROP TABLE IF EXISTS `core_device`;
CREATE TABLE `core_device` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `ip_address` varchar(128) NOT NULL,
  `bio` varchar(128) DEFAULT NULL,
  `port` int(10) NOT NULL,
  `hostId` int(10) NOT NULL,
  `deviceType` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for core_device_group
-- ----------------------------
DROP TABLE IF EXISTS `core_device_group`;
CREATE TABLE `core_device_group` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `bio` varchar(128) DEFAULT NULL,
  `hostGroup` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for core_device_interface
-- ----------------------------
DROP TABLE IF EXISTS `core_device_interface`;
CREATE TABLE `core_device_interface` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `dns` varchar(128) NOT NULL DEFAULT '',
  `deviceId` bigint(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `port` int(20) NOT NULL,
  `type` int(10) NOT NULL,
  `isDefault` int(10) NOT NULL DEFAULT '0',
  `interfaceId` int(20) NOT NULL,
  `useIp` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for core_dict
-- ----------------------------
DROP TABLE IF EXISTS `core_dict`;
CREATE TABLE `core_dict` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `createUserId` bigint(20) DEFAULT NULL,
  `type` varchar(128) NOT NULL,
  `code` bigint(20) NOT NULL DEFAULT '0',
  `text` varchar(64) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `isSystem` int(11) NOT NULL DEFAULT '0',
  `parentId` bigint(20) NOT NULL DEFAULT '0',
  `iconUrl` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for core_monitor_item
-- ----------------------------
DROP TABLE IF EXISTS `core_monitor_item`;
CREATE TABLE `core_monitor_item` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `monitorKey` varchar(128) NOT NULL,
  `itemName` varchar(50) NOT NULL,
  `bio` varchar(128) NOT NULL,
  `unit` varchar(20) DEFAULT NULL,
  `type` int(10) NOT NULL,
  `valueType` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for device_link
-- ----------------------------
DROP TABLE IF EXISTS `device_link`;
CREATE TABLE `device_link` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `type` varchar(128) NOT NULL,
  `target` bigint(20) NOT NULL,
  `deviceId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for device_monitor_item
-- ----------------------------
DROP TABLE IF EXISTS `device_monitor_item`;
CREATE TABLE `device_monitor_item` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `status` int(10) NOT NULL DEFAULT '0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `deviceId` bigint(20) NOT NULL,
  `monitorItemId` bigint(20) NOT NULL,
  `dayle` varchar(50) NOT NULL,
  `itemId` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for flyway_schema_history
-- ----------------------------
DROP TABLE IF EXISTS `flyway_schema_history`;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for worker_node
-- ----------------------------
DROP TABLE IF EXISTS `worker_node`;
CREATE TABLE `worker_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `host_name` varchar(64) NOT NULL COMMENT 'host name',
  `port` varchar(64) NOT NULL COMMENT 'port',
  `type` int(11) NOT NULL COMMENT 'node type: ACTUAL or CONTAINER',
  `launch_date` date NOT NULL COMMENT 'launch date',
  `modified` datetime NOT NULL COMMENT 'modified time',
  `created` datetime NOT NULL COMMENT 'created time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
