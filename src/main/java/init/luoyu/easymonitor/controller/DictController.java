package init.luoyu.easymonitor.controller;

import init.luoyu.easymonitor.base.model.ResponseData;
import init.luoyu.easymonitor.generator.IdWorker;
import init.luoyu.easymonitor.server.dict.business.enums.DictTypeEnum;
import init.luoyu.easymonitor.server.dict.repository.model.CoreDict;
import init.luoyu.easymonitor.server.dict.repository.service.ICoreDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LuoYu
 * date 2021/5/25
 */
@RestController
@RequestMapping(value = "/api/monitor/dict")
@Api(tags = "字典模块")
public class DictController {


    @Resource
    private ICoreDictService coreDictService;


    @GetMapping(value = "/appcollectioncreate")
    @ApiOperation(value = "创建应用集字典")
    @ApiImplicitParam(name = "name",value = "应用集名称",required = true)
    public ResponseData<Long> createApplicationCollection(@RequestParam(value = "name") String name) {
        CoreDict createCoreDict = new CoreDict();
        createCoreDict.setCode(IdWorker.getId());
        createCoreDict.setText(name);
        createCoreDict.setType(DictTypeEnum.DICT_TYPE_APPLICATION_COLLECTION.getType());
        createCoreDict.setIsSystem(1);
        Long id = IdWorker.getId();
        createCoreDict.setId(id);
        boolean save = coreDictService.save(createCoreDict);
        return ResponseData.success(id);
    }










}
