package init.luoyu.easymonitor.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import init.luoyu.easymonitor.base.model.ResponseData;
import init.luoyu.easymonitor.base.utils.CommonUtil;
import init.luoyu.easymonitor.base.utils.SpringContextUtil;
import init.luoyu.easymonitor.server.item.business.model.input.AddMonitorInput;
import init.luoyu.easymonitor.server.item.business.model.input.BulkDeviceItemInput;
import init.luoyu.easymonitor.server.item.business.service.IInterDeviceMonitorItemService;
import init.luoyu.easymonitor.server.item.business.service.IMonitorItemService;
import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;
import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>监控配置Controller</p>
 * @author LuoYu
 * 2021/5/5
 **/
@RestController
@RequestMapping(value = "/api/monitor/setting")
@Api(tags = "监控配置模块")
public class MonitorSettingController {




    @Resource
    private IInterDeviceMonitorItemService iInterDeviceMonitorItemService;


    @PostMapping(value = "/addmonitor")
    @ApiOperation(value = "添加监控项")
    public ResponseData<Boolean> addMonitor(@RequestBody @Valid AddMonitorInput input){
        return new ResponseData<>(iInterDeviceMonitorItemService.addOrUpdateHostItem(input));
    }


    @PostMapping(value = "/bulkdeleteitem")
    @ApiOperation(value = "批量删除设备的监控项")
    public ResponseData<Boolean> bulkDelete(@RequestBody @Valid BulkDeviceItemInput input) {
          return new ResponseData<>(iInterDeviceMonitorItemService.bulkDeleteHostItem(input.getDeviceId(),input.getItemIds()));
    }



    @GetMapping(value = "/getandfilteritem")
    @ApiOperation(value = "获取并过滤设备监控项")
    public ResponseData<List<MonitorItemDto>> getAndFilterMonitorItem(@RequestParam(value = "deviceId") Long deviceId) {
        QueryWrapper<CoreMonitorItem> itemQuery = new QueryWrapper<>();
        itemQuery.select("id","itemName","bio").eq("isDeleted",false);
        IMonitorItemService monitorItemService = SpringContextUtil.getBean(IMonitorItemService.class);
        List<CoreMonitorItem> items = monitorItemService.getMapper().selectList(itemQuery);
        if(CommonUtil.isEmptyCollection(items)) {
            return ResponseData.success(null);
        }
        ModelMapper modelMapper = new ModelMapper();
        List<MonitorItemDto> dtoList = CommonUtil.switcher(items,v->modelMapper.map(v,MonitorItemDto.class));
        QueryWrapper<DeviceMonitorItem> deviceItemQuery = new QueryWrapper<>();
        deviceItemQuery.select("id","monitorItemId").eq("deviceId",deviceId);
        List<DeviceMonitorItem> monitorItems = iInterDeviceMonitorItemService.getMapper().selectList(deviceItemQuery);
        if(CommonUtil.isEmptyCollection(monitorItems)) {
            return ResponseData.success(dtoList);
        }
        Map<Long, DeviceMonitorItem> itemMap = monitorItems.stream().collect(Collectors.toMap(DeviceMonitorItem::getMonitorItemId, Function.identity()));
        dtoList.forEach(v->{
            DeviceMonitorItem item = itemMap.get(v.getId());
            if(item != null) {
                v.setChecked(true);
            }
        });
        return ResponseData.success(dtoList);
    }






}

@Data
@ApiModel(value = "监控项实体")
class MonitorItemDto {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "监控项名称")
    private String itemName;

    @ApiModelProperty(value = "监控项描述")
    private String bio;

    @ApiModelProperty(value = "是否已经选择")
    private Boolean checked = false;

}







