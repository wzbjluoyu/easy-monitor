package init.luoyu.easymonitor.controller;
import init.luoyu.easymonitor.base.model.ResponseData;
import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.server.device.business.service.IDeviceService;
import init.luoyu.easymonitor.server.item.business.model.dto.MonitorItemDataDTO;
import init.luoyu.easymonitor.server.item.business.service.IMonitorItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>监控数据controller</p>
 * @author LuoYu
 * date 2021/5/13
 */
@RestController
@RequestMapping(value = "/api/monitor/data")
@Api(tags = "监控数据获取模块")
public class MonitorDataController {


    @Resource
    private IDeviceService deviceService;


    @Resource
    private IMonitorItemService monitorItemService;




    @GetMapping(value = "/getmonitordata")
    @ApiOperation(value = "获取监控数据")
    @ApiImplicitParam(name = "deviceId",value = "设备id",required = true)
    public ResponseData<List<MonitorItemDataDTO>> getMonitorData(@RequestParam(value = "deviceId") Long deviceId) {
        ServiceResult<List<MonitorItemDataDTO>> data = monitorItemService.getMonitorDataByDeviceId(deviceId);
        return ResponseData.success(data.getData());
    }







}
