package init.luoyu.easymonitor.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import init.luoyu.easymonitor.base.model.ResponseData;
import init.luoyu.easymonitor.base.model.ResponsePage;
import init.luoyu.easymonitor.base.model.input.PageInput;
import init.luoyu.easymonitor.base.utils.CommonUtil;
import init.luoyu.easymonitor.server.item.business.model.base.CreateCoreMonitorItem;
import init.luoyu.easymonitor.server.item.business.model.dto.MonitorItemDetailDto;
import init.luoyu.easymonitor.server.item.business.service.IInterDeviceMonitorItemService;
import init.luoyu.easymonitor.server.item.business.service.IMonitorItemService;
import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;
import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;
import io.swagger.annotations.*;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author LuoYu
 * 2021/5/5
 **/
@RestController
@RequestMapping(value = "/api/item")
@Api(tags = "监控项模块")
@CrossOrigin
public class MonitorItemController {



    @Resource
    private IMonitorItemService monitorItemService;

    @Resource
    private IInterDeviceMonitorItemService iInterDeviceMonitorItemService;


    @PostMapping(value = "/create")
    @ApiOperation(value = "创建监控项")
    public ResponseData<Long> create(@RequestBody @Valid CreateItemInput create) {
        ModelMapper modelMapper = new ModelMapper();
        CreateCoreMonitorItem monitorItem = modelMapper.map(create, CreateCoreMonitorItem.class);
        return new ResponseData<>(monitorItemService.save(monitorItem));
    }



    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "删除监控项")
    @ApiImplicitParam(name = "id",value = "监控项id",required = true)
    public ResponseData<Boolean> deleteById(@RequestParam(value = "id") Long id){
        QueryWrapper<DeviceMonitorItem> itemQueryWrapper = new QueryWrapper<>();
        itemQueryWrapper.select("id").eq("isDeleted",false).eq("monitorItemId",id);
        List<DeviceMonitorItem> items = iInterDeviceMonitorItemService.getMapper().selectList(itemQueryWrapper);
        if(!CommonUtil.isEmptyCollection(items)) {
            return ResponseData.error("该监控项已经添加了监控，无法删除");
        }
        return new ResponseData<>(monitorItemService.delete(id));
    }






    @PostMapping(value = "/page")
    @ApiOperation(value = "分页获取监控项列表")
    public ResponsePage<MonitorItemDetailDto> page(@RequestBody @Valid PageInput input) {
        Page<CoreMonitorItem> page = new Page<>(input.getPageIndex(),input.getPageSize());
        IPage<CoreMonitorItem> iPage = monitorItemService.getMapper().page(page);
        ModelMapper modelMapper = new ModelMapper();
        List<MonitorItemDetailDto> switcher = CommonUtil.switcher(iPage.getRecords(), v -> modelMapper.map(v, MonitorItemDetailDto.class));
        ResponsePage<MonitorItemDetailDto> responsePage = new ResponsePage<>();
        responsePage.setData(switcher);
        responsePage.setTotal(CommonUtil.isEmptyCollection(switcher) ? 0L : iPage.getTotal());
        return responsePage;
    }











}

@Data
@ApiModel(description = "创建监控项表单")
class CreateItemInput {

    @ApiModelProperty(value = "监控键",required = true)
    @NotBlank(message = "监控键不能为空")
    private String monitorKey;

    @ApiModelProperty(value = "监控项名称",required = true)
    @NotBlank(message = "监控键不能为空")
    private String itemName;

    @ApiModelProperty(value = "监控项描述",required = true)
    @NotBlank(message = "监控键不能为空")
    private String bio;

    @ApiModelProperty(value = "监控项单位")
    private String unit;

    @ApiModelProperty(value = "监控类型  0-客户端类型",required = true)
    @NotNull(message = "监控类型不能为空")
    private Integer type;

    @ApiModelProperty(value = "监控项返回值类型  0-数字浮点 1-字符串  2-日志  3-数字无正负  4-文本 ",required = true)
    @NotNull(message = "监控项返回数据类型不能为空")
    private Integer valueType;
}
