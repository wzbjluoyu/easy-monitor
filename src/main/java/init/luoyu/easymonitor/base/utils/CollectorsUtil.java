package init.luoyu.easymonitor.base.utils;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.function.Function;

/**
 * @author LuoYu
 * date 2021/5/19
 */
public class CollectorsUtil {



    public static <R,T> List<T> switcher(List<R> rList, Function<R,T> function) {
        List<T> list = Lists.newArrayList();
        if(CommonUtil.isEmptyCollection(rList)) {
            return list;
        }
        for (R r : rList) {
            T t = function.apply(r);
            list.add(t);
        }
        return list;
    }






}
