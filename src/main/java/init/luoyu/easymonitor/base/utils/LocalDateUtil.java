package init.luoyu.easymonitor.base.utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * @author LuoYu
 * date 2021/5/13
 */
public class LocalDateUtil {


    /**
     * <p>时间戳转换为日期时间</p>
     * @param timestamp 时间戳
     * @return          日期时间
     */
    public static LocalDateTime timestampToLocalDateTime(Long timestamp) {
        return LocalDateTime.ofEpochSecond(timestamp,0, ZoneOffset.ofHours(8));
    }







}
