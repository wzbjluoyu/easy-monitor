package init.luoyu.easymonitor.base.utils;

import com.google.common.collect.Lists;

import java.util.*;
import java.util.function.Function;

/**
 * <p>通用工具类</p>
 * @author LuoYu
 * date 2021/4/19
 */
public class CommonUtil {


    /**
     * <p>集合是否为空</p>
     * @param collection  集合
     */
    public static <T> boolean isEmptyCollection(Collection<T> collection){
        return (collection == null || collection.isEmpty());
    }


    /**
     * <p>字符串是否为空</p>
     * @param str  字符串
     * @return      true-空字符串   false-不是空字符串
     */
    public static boolean isEmptyStr(String str){
        return (str == null || str.length() == 0);
    }


    /**
     * <p>是否为空Map</p>
     * @param map map
     * @return  true-空 false-不为空
     */
    public static <K,V> boolean isEmptyMap(Map<K,V> map){
        return (map == null || map.isEmpty());
    }


    /**
     * <p>获取UUID</p>
     * @return  id
     */
    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }


    /**
     * <p>获取多个UUID</p>
     * @param num  获取数量
     * @return     uuidList
     */
    public static List<String> getUUIDs(Integer num) {
        Objects.requireNonNull(num,"获取UUID数量的值不能为空");
        if(num <= 0) {
            throw new IllegalArgumentException("无效的数量");
        }
        List<String> list = Lists.newArrayList();
        for(int i=0;i<num;i++) {
            list.add(getUUID());
        }
        return list;
    }


    /**
     * <p>数据转换</p>
     * @param list          源列表
     * @param function      转换方法
     * @param <R>           源类型
     * @param <T>           目标类型
     * @return              目录类型列表
     */
    public static<R,T> List<T> switcher(List<R> list, Function<R,T> function) {
        List<T> tList = Lists.newArrayList();
        if(CommonUtil.isEmptyCollection(list)) {
            return tList;
        }
        for (R r : list) {
            tList.add(function.apply(r));
        }
        return tList;
    }











}
