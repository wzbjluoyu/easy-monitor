package init.luoyu.easymonitor.base.enums;

/**
 * @author LuoYu
 * date 2021/4/13
 **/
public interface BaseEnum {


    /**
     * 获取枚举值
     * @return  value
     */
    Object getValue();

    /**
     * 获取枚举中文显示名称
     * @return  name
     */
    String getText();

    /**
     * 获取枚举下标
     * @return  index
     */
    Integer getIndex();







}
