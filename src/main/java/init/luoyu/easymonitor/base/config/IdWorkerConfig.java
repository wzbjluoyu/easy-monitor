package init.luoyu.easymonitor.base.config;

import init.luoyu.easymonitor.generator.impl.CachedUidGenerator;
import init.luoyu.easymonitor.generator.worker.DisposableWorkerIdAssigner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>idWorker配置类</p>
 * @author LuoYu
 * 2021/4/12
 **/
@Configuration
public class IdWorkerConfig {


    @Bean
    public DisposableWorkerIdAssigner idWorkerAssigner(){
        return new DisposableWorkerIdAssigner();
    }


    @Bean
    public CachedUidGenerator generator(DisposableWorkerIdAssigner idWorkerAssigner){
        CachedUidGenerator generator = new CachedUidGenerator();
        generator.setWorkerIdAssigner(idWorkerAssigner);
        generator.setEpochStr("2020-04-12");
        return generator;
    }




}
