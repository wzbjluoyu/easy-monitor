package init.luoyu.easymonitor.base.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author LuoYu
 * 2021/4/8
 **/
@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {


    @Bean
    public Docket createRestApi(){
        log.info("配置Swagger2....");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("init.luoyu.easymonitor.controller"))
                .paths(PathSelectors.any())
                .build();


    }


    private ApiInfo apiInfo(){

        return new ApiInfoBuilder()
                .title("Easy监控系统")
                .description("基于Zabbix的运维监控系统")
                .termsOfServiceUrl("http://localhost:80/")
                .version("1.0")
                .build();
    }





}
