package init.luoyu.easymonitor.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.service.IService;
import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.model.input.CreateInput;
import init.luoyu.easymonitor.base.model.input.UpdateInput;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>封装基础查询Service</p>
 * @author LuoYu
 * date 2021/4/13
 **/
public interface IBaseService<T extends Model<T>,Mapper extends BaseMapper<T>,Create extends CreateInput,Update extends UpdateInput> {


    /**
     * <p>创建</p>
     * @param create  实体
     * @return   实体Id
     */
    @Transactional(rollbackFor = Exception.class)
    ServiceResult<Long> save(Create create);


    /**
     * <p>批量保存</p>
     * @param list  列表
     * @return      boolean
     */
    ServiceResult<Boolean> bulkSave(List<Create> list);


    /**
     * <p>获取实体</p>
     * @param id
     * @return
     */
    ServiceResult<T> get(Long id);


    /**
     * <p>获取数据实体</p>
     * @param id  id
     * @param fields   字段名称
     * @return          实体
     */
    T getById(Long id,String... fields);



    /**
     * <p>删除</p>
     * @param id  id
     * @return    是否删除成功
     */
    @Transactional(rollbackFor = Exception.class)
    ServiceResult<Boolean> delete(Long id);


    /**
     * <p>更新</p>
     * @param update  实体
     * @return   是否更新成功
     */
    ServiceResult<Boolean> update(Update update);


    /**
     * <p>条件查询列表</p>
     * @param queryWrapper  条件查询
     * @return      list
     */
    ServiceResult<List<T>> list(QueryWrapper<T> queryWrapper);


    /**
     * 实体是否存在
     * @param id  id
     * @return    true-存在 false-不存在
     */
    boolean hasExist(Long id);


    /**
     * <p>获取mapper</p>
     * @return  持久层Mapper
     */
    Mapper getMapper();


    /**
     * <p>获取Mybatis自带的Service</p>
     * @return  service
     */
    IService<T> getService();




}
