package init.luoyu.easymonitor.base.model.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author LuoYu
 * date 2021/4/19
 */
@ApiModel(value = "分页对象")
@Data
public class PageInput {


    @ApiModelProperty(value = "分页下标",required = true)
    @NotNull(message = "分页页码不能为空")
    @Min(value = 1,message = "页码不能小于1")
    private Integer pageIndex;


    @ApiModelProperty(value = "每页数量",required = true)
    @NotNull(message = "每页数量不能为空")
    @Min(value = 1,message = "每页数量不能小于1")
    @Max(value = 500,message = "每页数量不能大于500")
    private Integer pageSize;


    public PageInput(Integer pageIndex,  Integer pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    public PageInput() {
    }
}
