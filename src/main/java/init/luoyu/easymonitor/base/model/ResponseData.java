package init.luoyu.easymonitor.base.model;

import init.luoyu.easymonitor.base.constant.CommonConstant;
import lombok.Data;

/**
 * <p>控制层返回数据对象</p>
 * @author LuoYu
 * 2021/5/5
 **/
@Data
public class ResponseData<T> {



    /**
     * 状态码 0-成功  500-错误
     */
    private int code = CommonConstant.serviceSuccess;


    /**
     * 返回消息
     */
    private String message = "success";


    /**
     * 返回数据
     */
    private T data;

    public ResponseData(ServiceResult<T> result) {
        this.code = result.getCode();
        this.message = result.getMessage();
        this.data = result.getData();
    }

    public ResponseData() {
    }

    /**
     * <p>返回成功数据</p>
     * @param data  成功数据
     * @param <T>   返回类型
     * @return      instance
     */
    public static<T> ResponseData<T> success(T data){
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setData(data);
        return responseData;
    }


    /**
     * <p>返回错误数据</p>
     * @param msg   错误消息
     * @param <T>   返回类型
     * @return      instance
     */
    public static <T> ResponseData<T> error(String msg) {
        ResponseData<T> responseData = new ResponseData<>();
        responseData.setCode(CommonConstant.serviceError);
        responseData.setMessage(msg);
        return responseData;
    }



    public boolean isSuccess() {
        return this.code == CommonConstant.serviceSuccess;
    }




}
