package init.luoyu.easymonitor.base.model;

import init.luoyu.easymonitor.base.constant.CommonConstant;
import lombok.Data;

import java.util.List;

/**
 * <p>返回分页数据</p>
 * @author LuoYu
 * date 2021/5/12
 */
@Data
public class ResponsePage<T> {


    /**
     * 状态码 0-成功  500-错误
     */
    private int code = CommonConstant.serviceSuccess;


    /**
     * 返回消息
     */
    private String message = "success";


    /**
     * 返回数据
     */
    private List<T> data;


    /**
     * 总数
     */
    private Long total;


    public boolean isSuccess() {
        return this.code == CommonConstant.serviceSuccess;
    }


}
