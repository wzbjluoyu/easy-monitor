package init.luoyu.easymonitor.base.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>添加下拉框实体类</p>
 * @author LuoYu
 * date 2021/5/19
 */
@Data
@ApiModel(value = "下拉框实体")
public class SelectDto  implements Serializable {


    @ApiModelProperty(value = "id值，如果该实体对应的是一个常量  则id值可能不存在")
    private Long id;

    @ApiModelProperty(value = "下拉框的value属性值")
    private String key;

    @ApiModelProperty(value = "下拉框的label属性值")
    private String label;

}
