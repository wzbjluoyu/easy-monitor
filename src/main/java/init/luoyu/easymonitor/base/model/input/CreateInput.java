package init.luoyu.easymonitor.base.model.input;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author LuoYu
 * 2021/4/19
 **/
public class CreateInput implements Serializable {

    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "创建人")
    private Long createUserId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }
}
