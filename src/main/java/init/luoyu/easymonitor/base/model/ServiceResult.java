package init.luoyu.easymonitor.base.model;


import init.luoyu.easymonitor.base.constant.CommonConstant;

/**
 * <p>service层返回结果</p>
 * @author LuoYu
 * date 2021/4/19
 */
public class ServiceResult<T> {

    /**
     * 状态码 0-成功  500-错误
     */
    private int code = CommonConstant.serviceSuccess;


    /**
     * 返回消息
     */
    private String message = "success";


    /**
     * 返回数据
     */
    private T data;


    public ServiceResult(T data) {
        this.data = data;
    }


    public ServiceResult(String message) {
        this.message = message;
        this.code = CommonConstant.serviceError;
    }

    /**
     * <p>成功</p>
     * @param data  数据
     * @param <T>   类型
     * @return      result
     */
    public static <T> ServiceResult<T> success(T data){
        return new ServiceResult<>(data);
    }


    /**
     * <p>错误</p>
     * @param message 错误消息
     * @param <T>     类型
     * @return        result
     */
    public static <T> ServiceResult<T> error(String message){
        return new ServiceResult<>(message);
    }


    /**
     * <p>是否处理成功</p>
     * @return  true-成功  false-失败
     */
    public boolean isSuccess(){
        return this.code == CommonConstant.serviceSuccess;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
