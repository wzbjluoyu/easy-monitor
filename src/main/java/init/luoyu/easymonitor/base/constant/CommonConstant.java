package init.luoyu.easymonitor.base.constant;

/**
 * @author LuoYu
 * date 2021/4/19
 **/
public interface CommonConstant {

    /**
     * service层处理返回状态码 成功
     */
    int serviceSuccess = 0;

    /**
     * service层处理返回状态码 错误
     */
    int serviceError = 500;


    /**
     * 状态-开启
     */
    int STATUS_ENABLE = 0;

    /**
     * 状态-关闭
     */
    int STATUS_DISABLE = 1;




}
