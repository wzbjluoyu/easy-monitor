package init.luoyu.easymonitor.base;

import init.luoyu.easymonitor.base.properties.ZabbixProperties;
import init.luoyu.easymonitor.base.utils.SpringContextUtil;
import init.luoyu.easymonitor.zabbix.server.ZabbixServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author LuoYu
 * date 2021/4/28
 */
@Component
public class ContentAfterRunner  implements ApplicationRunner {


    private Logger logger = LoggerFactory.getLogger(ContentAfterRunner.class);



    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Spring组件加载完毕....");
        new Thread(this::initZabbix).start();

    }



    public void initZabbix(){
        ZabbixProperties bean = SpringContextUtil.getBean(ZabbixProperties.class);
        logger.info("获取到Zabbix请求地址 ----> url:{}",bean.getUrl());
        ZabbixServer.url = bean.getUrl();
        ZabbixServer.username = bean.getUser();
        ZabbixServer.password = bean.getPassword();
    }





}
