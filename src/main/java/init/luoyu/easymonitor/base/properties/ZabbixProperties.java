package init.luoyu.easymonitor.base.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author LuoYu
 * date 2021/4/28
 */
@Configuration
@ConfigurationProperties(prefix = "zabbix")
@Data
public class ZabbixProperties {


    /**
     * zabbix请求地址
     */
    private String url;

    /**
     * 登录用户名
     */
    private String user;

    /**
     * 密码
     */
    private String password;






}
