package init.luoyu.easymonitor.server.item.business.model.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author LuoYu
 * 2021/5/5
 **/
@Data
@ApiModel(description = "添加监控项表单")
public class AddMonitorInput {


    @ApiModelProperty(value = "设备id",required = true)
    @NotNull(message = "设备id不能为空")
    private Long deviceId;


    @ApiModelProperty(value = "监控项列表",required = true)
    @NotEmpty(message = "监控项列表不能为空")
    @Valid
    private List<ItemSetting> itemSettings;




    @Data
    @ApiModel(description = "监控项设置")
    public static class ItemSetting {


        @ApiModelProperty(value = "监控项id",required = true)
        @NotNull(message = "监控项id不能为空")
        private Long itemId;


        @ApiModelProperty(value = "检测时间间隔 单位秒s  不传则默认为30s检测一次")
        private Integer dayle;
    }



}
