package init.luoyu.easymonitor.server.item.business.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import init.luoyu.easymonitor.zabbix.enums.ItemValueType;
import init.luoyu.easymonitor.zabbix.enums.ZabbixMonitorType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author LuoYu
 * date 2021/5/26
 */
@Data
@ApiModel(value = "监控项详情实体类型")
public class MonitorItemDetailDto {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "监控项名称")
    private String itemName;

    @ApiModelProperty(value = "监控项描述")
    private String bio;

    @ApiModelProperty(value = "监控项单位")
    private String unit;

    @ApiModelProperty(value = "监控类型")
    private Integer type;

    @ApiModelProperty(value = "监控类型 前端显示值")
    private String typeText;

    @ApiModelProperty(value = "监控项返回值类型")
    private Integer valueType;

    @ApiModelProperty(value = "监控项返回值类型 前端显示值")
    private String valueTypeText;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    public void setType(Integer type) {
        ZabbixMonitorType monitorType = ZabbixMonitorType.getEnum(type);
        if(monitorType != null) {
            this.typeText = monitorType.getBio();
        }
        this.type = type;
    }

    public void setValueType(Integer valueType) {
        ItemValueType valueType1 = ItemValueType.getEnum(valueType);
        if(valueType1 != null) {
            this.valueTypeText = valueType1.getBio();
        }
        this.valueType = valueType;
    }
}
