package init.luoyu.easymonitor.server.item.repository.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceGroup;
import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
public interface CoreMonitorItemMapper extends BaseMapper<CoreMonitorItem> {
    IPage<CoreMonitorItem> page(Page<CoreMonitorItem> page);
}
