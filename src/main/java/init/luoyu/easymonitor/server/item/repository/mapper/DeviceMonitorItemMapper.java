package init.luoyu.easymonitor.server.item.repository.mapper;

import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-05
 */
public interface DeviceMonitorItemMapper extends BaseMapper<DeviceMonitorItem> {

}
