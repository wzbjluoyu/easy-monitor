package init.luoyu.easymonitor.server.item.business.service;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.item.business.model.base.CreateCoreMonitorItem;
import init.luoyu.easymonitor.server.item.business.model.base.UpdateCoreMonitorItem;
import init.luoyu.easymonitor.server.item.business.model.dto.MonitorItemDataDTO;
import init.luoyu.easymonitor.server.item.repository.mapper.CoreMonitorItemMapper;
import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;

import java.util.List;

/**
 *
 * @author LuoYu
 * 2021/5/4
 **/
public interface IMonitorItemService extends IBaseService<CoreMonitorItem, CoreMonitorItemMapper, CreateCoreMonitorItem, UpdateCoreMonitorItem> {



   ServiceResult< List<MonitorItemDataDTO>> getMonitorDataByDeviceId(Long deviceId);





}
