package init.luoyu.easymonitor.server.item.repository.service.impl;

import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;
import init.luoyu.easymonitor.server.item.repository.mapper.DeviceMonitorItemMapper;
import init.luoyu.easymonitor.server.item.repository.service.IDeviceMonitorItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-05
 */
@Service
public class DeviceMonitorItemServiceImpl extends ServiceImpl<DeviceMonitorItemMapper, DeviceMonitorItem> implements IDeviceMonitorItemService {

}
