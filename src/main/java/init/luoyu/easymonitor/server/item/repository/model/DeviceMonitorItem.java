package init.luoyu.easymonitor.server.item.repository.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

/**
 * <p></p>
 *
 * @author LuoYu
 * @since 2021-05-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "DeviceMonitorItem对象", description = "")
public class DeviceMonitorItem extends Model<DeviceMonitorItem> {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    @TableField("createTime")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    @TableField("isDeleted")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private Integer status;
    @ApiModelProperty(value = "")
    @TableField("deleteTime")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    @TableField("updateTime")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    @TableField("updateUserId")
    private Long updateUserId;
    @ApiModelProperty(value = "")
    @TableField("createUserId")
    private Long createUserId;
    @ApiModelProperty(value = "")
    @TableField("deviceId")
    private Long deviceId;
    @ApiModelProperty(value = "")
    @TableField("monitorItemId")
    private Long monitorItemId;
    @ApiModelProperty(value = "")
    private String dayle;
    @ApiModelProperty(value = "")
    @TableField("itemId")
    private Integer itemId;


}
