package init.luoyu.easymonitor.server.item.repository.service.impl;

import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;
import init.luoyu.easymonitor.server.item.repository.mapper.CoreMonitorItemMapper;
import init.luoyu.easymonitor.server.item.repository.service.ICoreMonitorItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
@Service
public class CoreMonitorItemServiceImpl extends ServiceImpl<CoreMonitorItemMapper, CoreMonitorItem> implements ICoreMonitorItemService {

}
