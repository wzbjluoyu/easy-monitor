package init.luoyu.easymonitor.server.item.business.service;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.item.business.model.base.CreateDeviceMonitorItem;
import init.luoyu.easymonitor.server.item.business.model.base.UpdateDeviceMonitorItem;
import init.luoyu.easymonitor.server.item.business.model.input.AddMonitorInput;
import init.luoyu.easymonitor.server.item.repository.mapper.DeviceMonitorItemMapper;
import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;

import java.util.List;

/**
 * @author LuoYu
 * 2021/5/5
 **/
public interface IInterDeviceMonitorItemService extends IBaseService<DeviceMonitorItem, DeviceMonitorItemMapper, CreateDeviceMonitorItem, UpdateDeviceMonitorItem> {


    /**
     * <p>添加或更新设备监控项</p>
     * @param input  表单实体对象
     * @return       boolean
     */
    ServiceResult<Boolean> addOrUpdateHostItem(AddMonitorInput input);


    /**
     * <p>批量删除设备监控项</p>
     * @param ids  监控项id集合
     * @param deviceId   设备id
     * @return     boolean
     */
    ServiceResult<Boolean> bulkDeleteHostItem(Long deviceId,List<Long> ids);

}
