package init.luoyu.easymonitor.server.item.repository.service;

import init.luoyu.easymonitor.server.item.repository.model.CoreMonitorItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
public interface ICoreMonitorItemService extends IService<CoreMonitorItem> {

}
