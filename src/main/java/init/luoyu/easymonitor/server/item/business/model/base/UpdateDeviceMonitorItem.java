package init.luoyu.easymonitor.server.item.business.model.base;

import init.luoyu.easymonitor.base.model.input.UpdateInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;
import java.time.LocalDateTime;

/**
*
* <p></p>
* @author LuoYu
* @since 2021-05-05
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UpdateDeviceMonitorItem对象", description="")
public class UpdateDeviceMonitorItem extends UpdateInput {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private Integer status;
    @ApiModelProperty(value = "")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    private Long updateUserId;
    @ApiModelProperty(value = "")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private Long deviceId;
    @ApiModelProperty(value = "")
    private Long monitorItemId;
    @ApiModelProperty(value = "")
    private String dayle;
    @ApiModelProperty(value = "")
    private Integer itemId;


}
