package init.luoyu.easymonitor.server.item.repository.service;

import init.luoyu.easymonitor.server.item.repository.model.DeviceMonitorItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-05
 */
public interface IDeviceMonitorItemService extends IService<DeviceMonitorItem> {

}
