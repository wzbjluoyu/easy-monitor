package init.luoyu.easymonitor.server.item.business.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.time.LocalDateTime;

/**
 * @author LuoYu
 * date 2021/5/13
 */

@Data
@ApiModel(value = "监控项对应的监控数据实体")
public class MonitorItemDataDTO {


    @ApiModelProperty(value = "设备id")
    private Long deviceId;


    @ApiModelProperty(value = "监控项Id")
    private Long monitorItemId;


    @ApiModelProperty(value = "监控数据")
    private String monitorData;


    @ApiModelProperty(value = "监控项名称")
    private String itemName;

    @ApiModelProperty(value = "监控项描述")
    private String itemBio;


    @ApiModelProperty(value = "最新检测时间")
    private LocalDateTime lastCheckTime;


    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


}
