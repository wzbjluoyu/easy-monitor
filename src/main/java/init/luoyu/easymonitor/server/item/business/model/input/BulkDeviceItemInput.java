package init.luoyu.easymonitor.server.item.business.model.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/5/27
 */
@Data
@ApiModel(description = "批量删除设备监控项表单实体")
public class BulkDeviceItemInput {


    @ApiModelProperty(value = "设备id",required = true)
    @NotNull(message = "设备id不能为空")
    private Long deviceId;

    @ApiModelProperty(value = "监控项id集合",required = true)
    @NotNull(message = "监控项id集合不能为空")
    @NotEmpty(message = "监控项id集合不能为空")
    private List<Long> itemIds;



}
