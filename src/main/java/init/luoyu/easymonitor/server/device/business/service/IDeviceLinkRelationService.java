package init.luoyu.easymonitor.server.device.business.service;

import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.device.business.model.base.CreateLink;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateLink;
import init.luoyu.easymonitor.server.device.repository.mapper.DeviceLinkMapper;
import init.luoyu.easymonitor.server.device.repository.model.DeviceLink;

/**
 * @author LuoYu
 * 2021/5/4
 **/
public interface IDeviceLinkRelationService extends IBaseService<DeviceLink, DeviceLinkMapper, CreateLink, UpdateLink> {
}
