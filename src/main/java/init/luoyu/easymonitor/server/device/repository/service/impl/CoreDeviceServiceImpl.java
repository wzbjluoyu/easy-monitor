package init.luoyu.easymonitor.server.device.repository.service.impl;

import init.luoyu.easymonitor.server.device.repository.model.CoreDevice;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceMapper;
import init.luoyu.easymonitor.server.device.repository.service.ICoreDeviceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-28
 */
@Service
public class CoreDeviceServiceImpl extends ServiceImpl<CoreDeviceMapper, CoreDevice> implements ICoreDeviceService {

}
