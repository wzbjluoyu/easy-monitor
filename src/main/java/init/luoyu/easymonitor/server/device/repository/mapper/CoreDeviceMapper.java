package init.luoyu.easymonitor.server.device.repository.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import init.luoyu.easymonitor.server.device.repository.model.CoreDevice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-28
 */
public interface CoreDeviceMapper extends BaseMapper<CoreDevice> {



    IPage<CoreDevice> page(Page<CoreDevice> page);


}
