package init.luoyu.easymonitor.server.device.business.model.base;

import init.luoyu.easymonitor.base.model.input.UpdateInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;
import java.time.LocalDateTime;

/**
*
* <p></p>
* @author LuoYu
* @since 2021-05-04
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UpdateCoreDeviceInterface对象", description="")
public class UpdateCoreDeviceInterface extends UpdateInput {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private Integer status;
    @ApiModelProperty(value = "")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    private Long updateUserId;
    @ApiModelProperty(value = "")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private String dns;
    @ApiModelProperty(value = "")
    private Long deviceId;
    @ApiModelProperty(value = "")
    private String address;
    @ApiModelProperty(value = "")
    private Integer port;
    @ApiModelProperty(value = "")
    private Integer type;
    @ApiModelProperty(value = "")
    private Integer isDefault;
    @ApiModelProperty(value = "")
    private Integer interfaceId;
    @ApiModelProperty(value = "")
    private Integer useIp;


}
