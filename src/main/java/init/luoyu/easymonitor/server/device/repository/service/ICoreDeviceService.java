package init.luoyu.easymonitor.server.device.repository.service;

import init.luoyu.easymonitor.server.device.repository.model.CoreDevice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-28
 */
public interface ICoreDeviceService extends IService<CoreDevice> {

}
