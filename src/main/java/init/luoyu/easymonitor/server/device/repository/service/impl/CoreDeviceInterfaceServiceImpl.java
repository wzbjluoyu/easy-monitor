package init.luoyu.easymonitor.server.device.repository.service.impl;

import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceInterface;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceInterfaceMapper;
import init.luoyu.easymonitor.server.device.repository.service.ICoreDeviceInterfaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
@Service
public class CoreDeviceInterfaceServiceImpl extends ServiceImpl<CoreDeviceInterfaceMapper, CoreDeviceInterface> implements ICoreDeviceInterfaceService {

}
