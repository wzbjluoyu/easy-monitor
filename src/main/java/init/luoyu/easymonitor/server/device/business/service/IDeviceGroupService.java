package init.luoyu.easymonitor.server.device.business.service;


import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.device.business.model.base.CreateCoreDeviceGroup;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateCoreDeviceGroup;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceGroupMapper;
import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceGroup;

/**
 * @author LuoYu
 * date 2021/4/28
 **/
public interface IDeviceGroupService extends IBaseService<CoreDeviceGroup, CoreDeviceGroupMapper, CreateCoreDeviceGroup, UpdateCoreDeviceGroup> {



    ServiceResult<Long> createOrUpdate(UpdateCoreDeviceGroup group);









}
