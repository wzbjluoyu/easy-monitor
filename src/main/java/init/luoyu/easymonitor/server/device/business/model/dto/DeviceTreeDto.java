package init.luoyu.easymonitor.server.device.business.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author LuoYu
 * date 2021/5/25
 */
@Data
@ApiModel(value = "主机树形结构")
public class DeviceTreeDto {

    @ApiModelProperty(value = "节点显示名称")
    private String title;

    @ApiModelProperty(value = "是否为叶子节点")
    private Boolean isLeaf;

    @ApiModelProperty(value = "key值  唯一标识")
    private Long key;

    @ApiModelProperty(value = "子节点")
    private List<DeviceTreeDto> children;

}
