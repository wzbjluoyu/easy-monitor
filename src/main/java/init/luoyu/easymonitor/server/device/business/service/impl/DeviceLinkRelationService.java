package init.luoyu.easymonitor.server.device.business.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import init.luoyu.easymonitor.base.service.impl.BaseServiceImpl;
import init.luoyu.easymonitor.server.device.business.model.base.CreateLink;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateLink;
import init.luoyu.easymonitor.server.device.business.service.IDeviceLinkRelationService;
import init.luoyu.easymonitor.server.device.repository.mapper.DeviceLinkMapper;
import init.luoyu.easymonitor.server.device.repository.model.DeviceLink;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author LuoYu
 * 2021/5/4
 **/
@Service
@Slf4j
public class DeviceLinkRelationService extends BaseServiceImpl<DeviceLink, DeviceLinkMapper, CreateLink, UpdateLink> implements IDeviceLinkRelationService {
    public DeviceLinkRelationService(DeviceLinkMapper mapper, IService<DeviceLink> service) {
        super(mapper, service, DeviceLink.class, "设备关联");
    }
}
