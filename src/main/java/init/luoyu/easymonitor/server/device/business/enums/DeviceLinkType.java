package init.luoyu.easymonitor.server.device.business.enums;

/**
 * <p>设备关联类型枚举</p>
 * @author LuoYu
 * 2021/5/4
 **/
public enum DeviceLinkType {

    LINK_GROUP("group","关联主机群组"),
    LINK_MONITOR_ITEM("monitor_item","关联监控项"),
    LINK_DEVICE_INTERFACE("device_interface","关联设备监控接口"),
    ;


    DeviceLinkType(String type, String bio) {
        this.type = type;
        this.bio = bio;
    }

    private String type;

    private String bio;

    public String getType() {

        return this.type;
    }







}
