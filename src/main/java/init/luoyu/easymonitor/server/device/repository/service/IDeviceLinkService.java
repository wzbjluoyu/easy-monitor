package init.luoyu.easymonitor.server.device.repository.service;

import init.luoyu.easymonitor.server.device.repository.model.DeviceLink;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-03
 */
public interface IDeviceLinkService extends IService<DeviceLink> {

}
