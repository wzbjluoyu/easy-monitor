package init.luoyu.easymonitor.server.device.business.model.base;

import init.luoyu.easymonitor.base.model.input.UpdateInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;
import java.time.LocalDateTime;

/**
*
* <p></p>
* @author LuoYu
* @since 2021-04-28
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UpdateCoreDeviceGroup对象", description="")
public class UpdateCoreDeviceGroup extends UpdateInput {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private Integer status;
    @ApiModelProperty(value = "")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    private Long updateUserId;
    @ApiModelProperty(value = "")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private String name;
    @ApiModelProperty(value = "")
    private String bio;
    @ApiModelProperty(value = "")
    private Integer hostGroup;


}
