package init.luoyu.easymonitor.server.device.business.service;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.device.business.model.base.CreateCoreDevice;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateCoreDevice;
import init.luoyu.easymonitor.server.device.business.model.input.CreateOrUpdateDeviceInput;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceMapper;
import init.luoyu.easymonitor.server.device.repository.model.CoreDevice;

/**
 * @author LuoYu
 * date 2021/4/28
 **/
public interface IDeviceService extends IBaseService<CoreDevice, CoreDeviceMapper, CreateCoreDevice, UpdateCoreDevice> {


    /**
     * <p>创建或更新主机</p>
     * @param input  主机信息
     * @return                  主机Id
     */
    ServiceResult<Long> createOrUpdate(CreateOrUpdateDeviceInput input,Long operationUserId);




}
