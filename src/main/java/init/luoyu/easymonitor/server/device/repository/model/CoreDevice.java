package init.luoyu.easymonitor.server.device.repository.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

/**
 * <p></p>
 *
 * @author LuoYu
 * @since 2021-04-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "CoreDevice对象", description = "")
public class CoreDevice extends Model<CoreDevice> {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    @TableField("createTime")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    @TableField("isDeleted")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private Integer status;
    @ApiModelProperty(value = "")
    @TableField("deleteTime")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    @TableField("updateTime")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    @TableField("updateUserId")
    private Long updateUserId;
    @ApiModelProperty(value = "")
    @TableField("createUserId")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private String name;
    @ApiModelProperty(value = "")
    private String ipAddress;
    @ApiModelProperty(value = "")
    private String bio;
    @ApiModelProperty(value = "")
    private Integer port;
    @ApiModelProperty(value = "")
    @TableField("deviceType")
    private String deviceType;
    @ApiModelProperty(value = "")
    private Integer hostid;


}
