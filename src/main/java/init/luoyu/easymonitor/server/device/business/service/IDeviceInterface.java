package init.luoyu.easymonitor.server.device.business.service;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.IBaseService;
import init.luoyu.easymonitor.server.device.business.model.base.CreateCoreDeviceInterface;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateCoreDeviceInterface;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceInterfaceMapper;
import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceInterface;

/**
 * @author LuoYu
 * 2021/5/4
 **/
public interface IDeviceInterface extends IBaseService<CoreDeviceInterface, CoreDeviceInterfaceMapper, CreateCoreDeviceInterface, UpdateCoreDeviceInterface> {


    /**
     * <p>创建主机接口</p>
     * @param deviceId  设置id
     * @return           boolean
     */
    ServiceResult<Boolean> createByDevice(Long deviceId);






}
