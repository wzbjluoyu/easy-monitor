package init.luoyu.easymonitor.server.device.repository.service.impl;

import init.luoyu.easymonitor.server.device.repository.mapper.DeviceLinkMapper;
import init.luoyu.easymonitor.server.device.repository.model.DeviceLink;
import init.luoyu.easymonitor.server.device.repository.service.IDeviceLinkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-03
 */
@Service
public class DeviceLinkServiceImpl extends ServiceImpl<DeviceLinkMapper, DeviceLink> implements IDeviceLinkService {

}
