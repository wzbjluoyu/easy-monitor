package init.luoyu.easymonitor.server.device.business.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.impl.BaseServiceImpl;
import init.luoyu.easymonitor.server.device.business.model.base.CreateCoreDeviceGroup;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateCoreDeviceGroup;
import init.luoyu.easymonitor.server.device.business.service.IDeviceGroupService;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceGroupMapper;
import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceGroup;
import init.luoyu.easymonitor.zabbix.enums.ZabbixMethod;
import init.luoyu.easymonitor.zabbix.model.HostGroup;
import init.luoyu.easymonitor.zabbix.server.ZabbixResponse;
import init.luoyu.easymonitor.zabbix.server.ZabbixServer;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LuoYu
 * date 2021/4/28
 */
@Service
@Slf4j
public class DeviceGroupServiceImpl extends BaseServiceImpl<CoreDeviceGroup, CoreDeviceGroupMapper, CreateCoreDeviceGroup, UpdateCoreDeviceGroup> implements IDeviceGroupService {

    public DeviceGroupServiceImpl(CoreDeviceGroupMapper mapper, IService<CoreDeviceGroup> service) {
        super(mapper, service, CoreDeviceGroup.class, "主机群组");
    }

    @Override
    public ServiceResult<Long> createOrUpdate(UpdateCoreDeviceGroup group) {
        if(group.getId() == null){
            log.info("创建主机群组并提交Zabbix ----> 主机群组名称:{}",group.getName());
            HostGroup hostGroup = new HostGroup(group.getName());
            ZabbixResponse response = ZabbixServer.executeOut(hostGroup, ZabbixMethod.HOST_GROUP_CREATE);
            if(!response.isSuccess()) {
                log.error("请求Zabbix创建主机群组时发生错误 ----> Error:{}",response.getMsg());
                return ServiceResult.error(response.getMsg());
            }
            JSONArray ids = JSONObject.parseArray(response.getDataMap().get("groupids"));
            Integer groupId = ids.getInteger(0);
            if(groupId == null){
                log.error("请求Zabbix创建主机群组发生错误 ----->Error:未返回主机群组Id");
                return ServiceResult.error("发生错误，请联系管理员");
            }
            group.setHostGroup(groupId);
            CreateCoreDeviceGroup createCoreDeviceGroup = new ModelMapper().map(group, CreateCoreDeviceGroup.class);
            return save(createCoreDeviceGroup);
        }
        QueryWrapper<CoreDeviceGroup> groupQuery = new QueryWrapper<>();
        groupQuery.select("id","hostGroup").eq("id",group.getId());
        CoreDeviceGroup deviceGroup = this.getMapper().selectOne(groupQuery);
        if(deviceGroup == null) {
            log.warn("主机群组不存在 ----> 主机群组id:{}",group.getId());
            return ServiceResult.error("主机群组不存在");
        }
        //请求ZABBIX
        log.info("更新主机群组并提交Zabbix ----> 主机群组名称:{}",group.getName());
        Map<String,Object> updateMap = new HashMap<>();
        updateMap.put("groupid",deviceGroup.getHostGroup());
        updateMap.put("name",group.getName());
        ZabbixResponse zabbixResponse = ZabbixServer.update(updateMap, ZabbixMethod.HOST_GROUP_UPDATE);
        if(!zabbixResponse.isSuccess()) {
            log.error("请求Zabbix更新主机群组时发生错误 ----> Error:{}",zabbixResponse.getMsg());
            return ServiceResult.error(zabbixResponse.getMsg());
        }
        group.setHostGroup(deviceGroup.getHostGroup());
        ServiceResult<Boolean> update = this.update(group);
        return ServiceResult.success(group.getId());
    }
}
