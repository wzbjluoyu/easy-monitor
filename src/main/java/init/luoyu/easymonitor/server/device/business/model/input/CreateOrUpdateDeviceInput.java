package init.luoyu.easymonitor.server.device.business.model.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author LuoYu
 * 2021/5/2
 **/
@Data
@ApiModel(value = "创建或更新主机表单")
public class CreateOrUpdateDeviceInput {


    @ApiModelProperty(value = "设备Id 不传为新增")
    private Long id;

    @ApiModelProperty(value = "主机名称",required = true)
    @NotBlank(message = "主机名称不能为空")
    private String name;

    @ApiModelProperty(value = "监控端口 不传默认为10050")
    private String port;

    @ApiModelProperty(value = "主机地址 ip/域名",required = true)
    @NotBlank(message = "主机地址不能为空")
    private String address;


    @ApiModelProperty(value = "主机群组id",required = true)
    @NotNull(message = "主机群组id不能为空")
    private Long groupId;

}
