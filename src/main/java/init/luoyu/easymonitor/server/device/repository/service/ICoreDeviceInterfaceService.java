package init.luoyu.easymonitor.server.device.repository.service;

import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceInterface;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
public interface ICoreDeviceInterfaceService extends IService<CoreDeviceInterface> {

}
