package init.luoyu.easymonitor.server.device.business.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.google.common.collect.Lists;
import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.service.impl.BaseServiceImpl;
import init.luoyu.easymonitor.base.utils.SpringContextUtil;
import init.luoyu.easymonitor.server.device.business.model.base.CreateCoreDeviceInterface;
import init.luoyu.easymonitor.server.device.business.model.base.UpdateCoreDeviceInterface;
import init.luoyu.easymonitor.server.device.business.service.IDeviceInterface;
import init.luoyu.easymonitor.server.device.business.service.IDeviceService;
import init.luoyu.easymonitor.server.device.repository.mapper.CoreDeviceInterfaceMapper;
import init.luoyu.easymonitor.server.device.repository.model.CoreDevice;
import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceInterface;
import init.luoyu.easymonitor.zabbix.server.ZabbixServer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LuoYu
 * 2021/5/4
 **/
@Service
@Slf4j
public class DeviceInterfaceServiceImpl
        extends BaseServiceImpl<CoreDeviceInterface, CoreDeviceInterfaceMapper, CreateCoreDeviceInterface, UpdateCoreDeviceInterface>
        implements IDeviceInterface {

    public DeviceInterfaceServiceImpl(CoreDeviceInterfaceMapper mapper, IService<CoreDeviceInterface> service) {
        super(mapper, service, CoreDeviceInterface.class, "主机接口");
    }

    @Override
    public ServiceResult<Boolean> createByDevice(Long deviceId) {
        log.info("创建设备接口 ----> 设备id:{}",deviceId);
        IDeviceService deviceService = SpringContextUtil.getBean(IDeviceService.class);
        CoreDevice coreDevice = deviceService.getService().getById(deviceId);
        if(coreDevice == null) {
            log.error("设备不存在 ----> 设备id:{}",deviceId);
            return ServiceResult.error("设备不存在");
        }
        JSONArray jsonArray = ZabbixServer.getHostInterface(coreDevice.getHostid());
        if(jsonArray == null || jsonArray.size() == 0) {
            log.warn("该设备下未找到接口信息 ----> 设备id：{} -- 远程hostId:{}",deviceId,coreDevice.getHostid());
            return ServiceResult.success(true);
        }
        List<InterfaceItem> interfaceItems = jsonArray.toJavaList(InterfaceItem.class);
        List<CreateCoreDeviceInterface> interfaces = Lists.newArrayList();
        interfaceItems.forEach(v->{
            CreateCoreDeviceInterface deviceInterface = new CreateCoreDeviceInterface();
            deviceInterface.setAddress(v.getIp());
            deviceInterface.setDns(v.getDns());
            deviceInterface.setType(v.getType());
            deviceInterface.setPort(v.getPort());
            deviceInterface.setDeviceId(deviceId);
            deviceInterface.setUseIp(v.getUseip());
            deviceInterface.setInterfaceId(v.getInterfaceid());
            interfaces.add(deviceInterface);
        });
        return this.bulkSave(interfaces);
    }
}


@Data
class InterfaceItem {


    private Integer interfaceid;


    private Integer hostid;


    private Integer main;


    private Integer type;


    private Integer useip;


    private String ip;


    private String dns;

    private Integer port;

    private Integer bulk;












}