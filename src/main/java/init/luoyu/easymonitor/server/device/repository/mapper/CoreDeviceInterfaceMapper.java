package init.luoyu.easymonitor.server.device.repository.mapper;

import init.luoyu.easymonitor.server.device.repository.model.CoreDeviceInterface;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LuoYu
 * @since 2021-05-04
 */
public interface CoreDeviceInterfaceMapper extends BaseMapper<CoreDeviceInterface> {

}
