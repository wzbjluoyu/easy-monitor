/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserve.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package init.luoyu.easymonitor.server.idwork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import init.luoyu.easymonitor.server.idwork.model.WorkerNode;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * DAO for M_WORKER_NODE
 *
 * @author yutianbao
 */
@Repository
public interface WorkerNodeDAO extends BaseMapper<WorkerNode> {

    /**
     * Get {@link WorkerNode} by node host
     * 
     * @param host 主机
     * @param port  端口
     * @return
     */
    WorkerNode getWorkerNodeByHostPort(@Param("host") String host, @Param("port") String port);

    /**
     * Add {@link WorkerNode}
     * 
     * @param workerNode
     */
    void addWorkerNode(WorkerNode workerNode);

}
