package init.luoyu.easymonitor.server.dict.business.model.base;


import init.luoyu.easymonitor.base.model.input.UpdateInput;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;

import java.time.LocalDateTime;

/**
*
* <p></p>
* @author LuoYu
* @since 2021-04-17
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="UpdateCoreDict对象", description="")
public class UpdateCoreDict extends UpdateInput {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private String type;
    @ApiModelProperty(value = "")
    private Long code;
    @ApiModelProperty(value = "")
    private String text;
    @ApiModelProperty(value = "")
    private String description;
    @ApiModelProperty(value = "")
    private Integer isSystem;
    @ApiModelProperty(value = "")
    private Long parentId;
    @ApiModelProperty(value = "")
    private String iconUrl;
    @ApiModelProperty(value = "")
    private Long updateUserId;


}
