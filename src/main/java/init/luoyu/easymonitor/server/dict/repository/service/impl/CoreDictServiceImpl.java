package init.luoyu.easymonitor.server.dict.repository.service.impl;

import init.luoyu.easymonitor.server.dict.repository.model.CoreDict;
import init.luoyu.easymonitor.server.dict.repository.mapper.CoreDictMapper;
import init.luoyu.easymonitor.server.dict.repository.service.ICoreDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-14
 */
@Service
public class CoreDictServiceImpl extends ServiceImpl<CoreDictMapper, CoreDict> implements ICoreDictService {

}
