package init.luoyu.easymonitor.server.dict.repository.mapper;

import init.luoyu.easymonitor.server.dict.repository.model.CoreDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-14
 */
public interface CoreDictMapper extends BaseMapper<CoreDict> {

}
