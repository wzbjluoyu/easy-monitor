package init.luoyu.easymonitor.server.dict.repository.service;

import init.luoyu.easymonitor.server.dict.repository.model.CoreDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoYu
 * @since 2021-04-14
 */
public interface ICoreDictService extends IService<CoreDict> {

}
