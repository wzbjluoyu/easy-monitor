package init.luoyu.easymonitor.server.dict.business.enums;

import init.luoyu.easymonitor.base.utils.CommonUtil;

/**
 * @author LuoYu
 * date 2021/5/25
 **/
public enum DictTypeEnum {

    DICT_TYPE_APPLICATION_COLLECTION("application_collection","应用集");


    private String type;

    private String text;

    DictTypeEnum(String type, String text) {
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return this.type;
    }


    public DictTypeEnum getEnum(String type) {
        if(CommonUtil.isEmptyStr(type)) {
            return null;
        }
        DictTypeEnum[] enums = DictTypeEnum.values();
        for (DictTypeEnum typeEnum : enums) {
            if(typeEnum.getType().equals(type)) {
                return typeEnum;
            }
        }
        return null;
    }






}
