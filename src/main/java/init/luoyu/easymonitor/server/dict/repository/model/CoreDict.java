package init.luoyu.easymonitor.server.dict.repository.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;

/**
 * <p></p>
 *
 * @author LuoYu
 * @since 2021-04-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "CoreDict对象", description = "")
public class CoreDict extends Model<CoreDict> {


    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "")
    @TableField("createTime")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "")
    @TableField("isDeleted")
    private Boolean isDeleted;
    @ApiModelProperty(value = "")
    @TableField("deleteTime")
    private LocalDateTime deleteTime;
    @ApiModelProperty(value = "")
    @TableField("updateTime")
    private LocalDateTime updateTime;
    @ApiModelProperty(value = "")
    @TableField("createUserId")
    private Long createUserId;
    @ApiModelProperty(value = "")
    private String type;
    @ApiModelProperty(value = "")
    private Long code;
    @ApiModelProperty(value = "")
    private String text;
    @ApiModelProperty(value = "")
    private String description;
    @ApiModelProperty(value = "")
    @TableField("isSystem")
    private Integer isSystem;
    @ApiModelProperty(value = "")
    @TableField("parentId")
    private Long parentId;
    @ApiModelProperty(value = "")
    @TableField("iconUrl")
    private String iconUrl;
    @ApiModelProperty(value = "")
    @TableField("updateUserId")
    private Long updateUserId;


}
