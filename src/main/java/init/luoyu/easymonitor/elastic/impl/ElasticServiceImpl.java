package init.luoyu.easymonitor.elastic.impl;

import init.luoyu.easymonitor.elastic.ElasticService;
import org.springframework.stereotype.Service;

/**
 * @author LuoYu
 * date 2021/5/13
 */
@Service
public class ElasticServiceImpl<T> extends ElasticService<T> {
}
