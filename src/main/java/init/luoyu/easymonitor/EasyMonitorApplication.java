package init.luoyu.easymonitor;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan(value = {"init.luoyu.easymonitor.server.**.mapper"})
@EnableScheduling
@EnableTransactionManagement(proxyTargetClass = true)
public class EasyMonitorApplication {

    public static void main(String[] args) {

        SpringApplication.run(EasyMonitorApplication.class, args);
    }



}
