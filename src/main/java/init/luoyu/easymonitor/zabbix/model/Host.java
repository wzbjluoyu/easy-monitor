package init.luoyu.easymonitor.zabbix.model;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.utils.CommonUtil;
import init.luoyu.easymonitor.zabbix.server.ZabbixRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/4/23
 */
@Data
public class Host implements Serializable, ZabbixRequest {


    private Integer hostid;


    private String host;

    private List<InterFaceItem> interfaces;


    private List<GroupItem> groups;




    public Host setName(String name){
        this.host = name;
        return this;
    }


    public Host addInterface(String ip){
        if(this.interfaces == null){
            this.interfaces = new ArrayList<>();
        }
        this.interfaces.add(new InterFaceItem(ip));
        return this;
    }


    public Host addGroup(String id){
        if(this.groups == null){
            this.groups = new ArrayList<>();
        }
        this.groups.add(new GroupItem(id));
        return this;
    }

    @Override
    public ServiceResult<Boolean> paramsChecker() {
        if(CommonUtil.isEmptyStr(this.host)) {
            return ServiceResult.error("未输入主机名称");
        }
        if(CommonUtil.isEmptyCollection(this.interfaces)) {
            return ServiceResult.error("未设置主机ip");
        }
        if(CommonUtil.isEmptyCollection(this.groups)) {
            return ServiceResult.error("未设置主机群组");
        }
        return ServiceResult.success(true);
    }


    @Data
    public static class GroupItem {
        private String groupid;

        public GroupItem(String groupid) {
            this.groupid = groupid;
        }
    }




    @Data
    public static class InterFaceItem {

        private int type = 1;

        private int main=1;

        private int useip = 1;

        private String ip;

        private String port = "10050";

        private String dns = "";

        public InterFaceItem() {
        }

        public InterFaceItem(String ip) {
            this.ip = ip;
        }
    }


}
