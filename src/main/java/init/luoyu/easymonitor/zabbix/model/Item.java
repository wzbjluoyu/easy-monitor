package init.luoyu.easymonitor.zabbix.model;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.zabbix.server.ZabbixRequest;

import java.io.Serializable;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/4/26
 */
public class Item implements Serializable, ZabbixRequest {

    private String name;

    private String key_;

    private String hostid;

    private int type;

    private int value_type;


    private String interfaceid;

    private List<String> applications;


    private String delay;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey_() {
        return key_;
    }

    public void setKey_(String key_) {
        this.key_ = key_;
    }

    public String getHostid() {
        return hostid;
    }

    public void setHostid(String hostid) {
        this.hostid = hostid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getValue_type() {
        return value_type;
    }

    public void setValue_type(int value_type) {
        this.value_type = value_type;
    }

    public List<String> getApplications() {
        return applications;
    }

    public void setApplications(List<String> applications) {
        this.applications = applications;
    }

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }


    public String getInterfaceid() {
        return interfaceid;
    }

    public void setInterfaceid(String interfaceid) {
        this.interfaceid = interfaceid;
    }

    @Override
    public ServiceResult<Boolean> paramsChecker() {
        return ServiceResult.success(true);
    }
}
