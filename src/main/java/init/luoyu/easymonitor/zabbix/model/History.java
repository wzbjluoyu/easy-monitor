package init.luoyu.easymonitor.zabbix.model;

import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.zabbix.server.ZabbixRequest;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author LuoYu
 * date 2021/5/11
 */
@Data
public class History implements ZabbixRequest, Serializable {


    private String output = "extend";

    private List<String> itemids;

    private Integer limit = 10;

    private String sortorder = "DESC";

    private Integer history = 0;

    private String sortfield = "clock";



    @Override
    public ServiceResult<Boolean> paramsChecker() {
        return ServiceResult.success(true);
    }
}
