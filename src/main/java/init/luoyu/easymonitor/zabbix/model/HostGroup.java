package init.luoyu.easymonitor.zabbix.model;


import init.luoyu.easymonitor.base.model.ServiceResult;
import init.luoyu.easymonitor.base.utils.CommonUtil;
import init.luoyu.easymonitor.zabbix.server.ZabbixRequest;
import lombok.Data;

/**
 * @author LuoYu
 * date 2021/4/26
 */
@Data
public class HostGroup implements ZabbixRequest {

    private String name;


    public HostGroup(String name) {
        this.name = name;
    }


    @Override
    public ServiceResult<Boolean> paramsChecker() {
        if(CommonUtil.isEmptyStr(this.name)){
            return ServiceResult.error("主机群组名称不能为空");
        }
        return ServiceResult.success(true);
    }
}
