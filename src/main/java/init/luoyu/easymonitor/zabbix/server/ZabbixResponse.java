package init.luoyu.easymonitor.zabbix.server;

import init.luoyu.easymonitor.base.constant.CommonConstant;
import lombok.Data;

import java.util.Map;

/**
 * @author LuoYu
 * date 2021/4/28
 */
@Data
public class ZabbixResponse {


    private int code = CommonConstant.serviceSuccess;


    private Map<String,String> dataMap;


    private String msg;



    public boolean isSuccess(){
        return this.code == CommonConstant.serviceSuccess;
    }


    public static ZabbixResponse error(String msg){
        ZabbixResponse zabbixResponse = new ZabbixResponse();
        zabbixResponse.setCode(CommonConstant.serviceError);
        zabbixResponse.setMsg(msg);
        return zabbixResponse;
    }


    public static ZabbixResponse success(Map<String,String> dataMap){
        ZabbixResponse zabbixResponse = new ZabbixResponse();
        zabbixResponse.setDataMap(dataMap);
        return zabbixResponse;
    }




}
