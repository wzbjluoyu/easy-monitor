package init.luoyu.easymonitor.zabbix.server;


import init.luoyu.easymonitor.base.model.ServiceResult;

/**
 * <p>zabbix请求</p>
 * @author LuoYu
 * date 2021/4/25
 **/
public interface ZabbixRequest {


    /**
     * <p>参数校验</p>
     * @return 是否校验成功
     */
    ServiceResult<Boolean> paramsChecker();


}
