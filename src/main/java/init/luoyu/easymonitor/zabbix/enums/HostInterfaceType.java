package init.luoyu.easymonitor.zabbix.enums;

/**
 * <P>主机接口类型</P>
 * @author LuoYu
 * 2021/5/4
 **/
public enum HostInterfaceType {

    ZABBIX_AGENT(1,"普通客户端"),
    SNMP(2,"交换机接口"),
    IPMI(3,"智能型平台管理接口"),
    JMX(4,"Java管理扩展接口")
    ;




    public static HostInterfaceType getEnum(Integer type) {
        if(type == null) {
            return null;
        }
        HostInterfaceType[] types = HostInterfaceType.values();
        for (HostInterfaceType monitorType : types) {
            if(monitorType.getType().equals(type)) {
                return monitorType;
            }
        }
        return null;
    }



    HostInterfaceType(Integer type, String bio) {
        this.type = type;
        this.bio = bio;
    }



    public Integer getType() {
        return this.type;
    }

    private Integer type;

    private String bio;





}
