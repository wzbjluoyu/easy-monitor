package init.luoyu.easymonitor.zabbix.enums;

/**
 * <p>监控项监控数据类型</p>
 * @author LuoYu
 * 2021/5/5
 **/
public enum ItemValueType {
    NUMBER_FLOAT(0,"数字浮点型数据"),
    CHARACTER(1,"字符串类型"),
    LOG(2,"日志类型"),
    NUMBER_UNSIGNED(3,"数字无正负类型"),
    TEXT(4,"文本类型");


    ItemValueType(Integer type, String bio) {
        this.type = type;
        this.bio = bio;
    }


    public static ItemValueType getEnum(Integer type) {
        if(type == null) {
            return null;
        }
        ItemValueType[] values = ItemValueType.values();
        for (ItemValueType value : values) {
            if(value.getType().equals(type)) {
                return value;
            }
        }
        return null;
    }


    public Integer getType() {
        return this.type;
    }


    public String getBio() {
        return this.bio;
    }


    private Integer type;


    private String bio;







}
