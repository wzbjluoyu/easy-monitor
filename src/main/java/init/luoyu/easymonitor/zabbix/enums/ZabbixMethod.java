package init.luoyu.easymonitor.zabbix.enums;

/**
 * @author LuoYu
 * date 2021/4/23
 **/
public enum ZabbixMethod {


    CREATE_HOST("host.create","创建主机"),

    HOST_GROUP_CREATE("hostgroup.create","创建主机群组"),

    HOST_GROUP_UPDATE("hostgroup.update","更新主机群组"),

    DELETE_HOST("host.delete","删除主机"),

    UPDATE_HOST("host.update","更新主机"),

    GET_HOST("host.get","获取主机"),

    ITEM_CREATE("item.create","创建监控项"),

    ITEM_UPDATE("item.update","更新监控项"),

    ITEM_DELETE("item.delete","删除监控项"),

    HOST_INTERFACE_GET("hostinterface.get","获取主机接口"),

    USER_LOGIN("user.login","登录"),

    HISTORY_GET("history.get","获取历史数据");

    /**
     * 方法名称
     */
    private String method;

    /**
     * 方法描述
     */
    private String bio;


    ZabbixMethod(String method, String bio) {
        this.method = method;
        this.bio = bio;
    }


    public String getMethod(){
        return this.method;
    }








}
