package init.luoyu.easymonitor.zabbix.enums;

/**
 * <p>Zabbix监控接口协议类型</p>
 * @author LuoYu
 * 2021/5/4
 **/
public enum ZabbixMonitorType {
    ZABBIX_AGENT(0,"普通客户端监控类型");




    public static ZabbixMonitorType getEnum(Integer type) {
        if(type == null) {
            return null;
        }
        ZabbixMonitorType[] types = ZabbixMonitorType.values();
        for (ZabbixMonitorType monitorType : types) {
            if(monitorType.getType().equals(type)) {
                return monitorType;
            }
        }
        return null;
    }



    ZabbixMonitorType(Integer type, String bio) {
        this.type = type;
        this.bio = bio;
    }



    public Integer getType() {
        return this.type;
    }

    private Integer type;

    private String bio;

    public String getBio() {
        return this.bio;
    }






}
