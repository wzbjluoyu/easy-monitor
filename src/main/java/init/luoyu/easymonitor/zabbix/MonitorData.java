package init.luoyu.easymonitor.zabbix;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>监控数据实体</p>
 * @author LuoYu
 * date 2021/5/12
 */
@Data
public class MonitorData implements Serializable {


    /**
     * zabbix监控项id
     */
    private Integer itemId;


    /**
     * 设备id
     */
    private Long deviceId;


    /**
     * 监控项Id
     */
    private Long monitorItemId;


    /**
     * 监控数据
     */
    private String value;


    /**
     * 监控数据产生时的时间戳
     */
    private Long clock;

    /**
     * 数据类型
     */
    private Integer valueType;


}
