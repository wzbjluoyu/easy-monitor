package init.luoyu.easymonitor.generator;

import com.google.common.collect.Lists;
import init.luoyu.easymonitor.base.utils.SpringContextUtil;
import init.luoyu.easymonitor.generator.impl.CachedUidGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

/**
 * @author LuoYu
 * 2021/4/12
 **/
public class IdWorker {


    private static UidGenerator idWorker;

    private static Logger logger = LoggerFactory.getLogger(IdWorker.class);

    public static void init(){
        idWorker = SpringContextUtil.getBean(CachedUidGenerator.class);
    }





    /**
     * <p>获取id</p>
     * @return  id
     */
    public static long getId(){
        if(idWorker == null){
            init();
        }
        return idWorker.getUID();
    }


    /**
     * <p>获取多个id</p>
     * @param size  id数量
     * @return      id集合
     */
    public static List<Long> getIds(int size){
        if(size <= 0) {
            logger.error("生成id失败 ----> 生成数量{}不合法",size);
            throw new RuntimeException("id数量不合法");
        }
        List<Long> ids = Lists.newArrayList();
        for (int i=0; i< size;i++){
            ids.add(getId());
        }
        return ids;
    }










}
