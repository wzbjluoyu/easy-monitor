CREATE TABLE core_device (
	id                   bigint  NOT NULL    PRIMARY KEY,
	name                 varchar(100)  NOT NULL    ,
	ip_address           varchar(100)  NOT NULL    ,
	port                 int  NOT NULL    ,
	create_time          datetime  NOT NULL DEFAULT CURRENT_TIMESTAMP   ,
	create_user_id       bigint      
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

CREATE INDEX idx_core_device_create_user_id ON core_device ( create_user_id );

ALTER TABLE core_device MODIFY id bigint  NOT NULL   COMMENT '主键';

ALTER TABLE core_device MODIFY name varchar(100)  NOT NULL   COMMENT '主机名称';

ALTER TABLE core_device MODIFY port int  NOT NULL   COMMENT '监控数据端口';
