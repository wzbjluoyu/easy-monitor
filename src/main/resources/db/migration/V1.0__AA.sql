SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `has_default` bit(1) DEFAULT b'1',
  `is_deleted` bit(1) DEFAULT b'0',
  `locked` bit(1) DEFAULT b'0',
  `notes` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT b'1',
  `update_time` datetime DEFAULT NULL,
  `nick_name` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `id_card` varchar(50) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `true_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
