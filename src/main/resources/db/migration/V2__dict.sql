SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `core_dict`;
CREATE TABLE `core_dict` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
  `createUserId` bigint(20) DEFAULT NULL,
  `type` varchar(128) NOT NULL,
  `code` bigint(20) NOT NULL DEFAULT '0',
  `text` varchar(64) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `isSystem` int(11) NOT NULL DEFAULT '0',
  `parentId` bigint(20) NOT NULL DEFAULT '0',
  `iconUrl` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;