CREATE TABLE `core_device` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
`status` int(10) NOT NULL DEFAULT 0,
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
`updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `ip_address` varchar(128) NOT NULL,
  `bio` varchar(128) DEFAULT NULL,
   `port` int(10) NOT NULL,
   `hostId` int(10) NOT NULL,
    `deviceType` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;