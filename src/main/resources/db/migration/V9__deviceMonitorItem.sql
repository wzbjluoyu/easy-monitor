CREATE TABLE `device_monitor_item` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
`status` int(10) NOT NULL DEFAULT 0,
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
`updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `deviceId` bigint(20) NOT NULL,
  `monitorItemId` bigint (20) NOT NULL,
  `dayle` varchar(50) NOT NULL,
  `itemId` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;