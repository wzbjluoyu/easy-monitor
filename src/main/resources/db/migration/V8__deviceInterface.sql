CREATE TABLE `core_device_interface` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` bit(1) NOT NULL DEFAULT b'0',
`status` int(10) NOT NULL DEFAULT 0,
  `deleteTime` timestamp NULL DEFAULT NULL,
  `updateTime` timestamp NULL DEFAULT NULL,
`updateUserId` bigint(20) DEFAULT '0',
  `createUserId` bigint(20) DEFAULT NULL,
  `dns` varchar(128) NOT NULL default '',
  `deviceId` bigint(20) NOT NULL,
  `address` varchar (50) NOT NULL,
  `port` int(20) NOT NULL,
  `type` int(10) NOT NULL,
   `isDefault` int(10) NOT NULL DEFAULT 0,
   `interfaceId` int(20) NOT NULL,
    `useIp` int(10) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;