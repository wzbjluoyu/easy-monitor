package ${package.Entity};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;
import init.luoyu.input.UpdateInput;
import java.time.LocalDateTime;

/**
*
* <p>${table.comment!}</p>
* @author ${author}
* @since ${date}
*/
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="${entity}对象", description="${table.comment!}")
public class ${entity} extends UpdateInput{


<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    @ApiModelProperty(value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
</#list>
<#------------  END 字段循环遍历  ---------->


}
