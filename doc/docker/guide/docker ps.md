# docker ps指令

## 描述

列举容器

## 用法

> docker   ps  [Options]

关于此命令的使用示例，请参阅下面的示例部分

## Options

| 选项名称, 简写    | 默认值 | 描述                                                         |
| ----------------- | ------ | ------------------------------------------------------------ |
| `--all` , `-a`    |        | 显示所有的容器，包括已经关闭的容器，未启动成功的容器，如果不指定该选项则只显示正在运行的容器 |
| `--filter` , `-f` |        | 根据提供的条件过滤满足条件的容器                             |
| `--format`        |        | 使用Go模板美化打印容器                                       |
| `--last` , `-n`   | `-1`   | 显示最后创建的容器(包括所有状态)                             |
| `--latest` , `-l` |        | 显示最新创建的容器(包括所有状态)                             |
| `--no-trunc`      |        | 不截断输出                                                   |
| `--quiet` , `-q`  |        | 只显示容器id                                                 |
| `--size` , `-s`   |        | 显示文件大小                                                 |

## 示例

### 不截断输出(--no-trunc)

运行 __docker   ps   --no-trunc__ 查看容器

```
$ docker ps  --no-trunc
docker  ps指令展示容器时，为了保证返回结果格式一致，会把一些比较长的内容截断隐藏掉，使用上面的指令可全部展示
```

### 同时显示正在运行和已经停止的容器(-a)

默认情况下，docker ps命令只显示正在运行的容器。要查看所有容器，请使用-a(或--all)选项:

```
$ docker ps -a
```

### 显示容器的磁盘使用情况(-s)

```
$ docker ps -s

CONTAINER ID   IMAGE          COMMAND                  CREATED        STATUS       PORTS   NAMES        SIZE                                                                                      SIZE
e90b8831a4b8   nginx          "/bin/bash -c 'mkdir "   11 weeks ago   Up 4 hours           my_nginx     35.58 kB (virtual 109.2 MB)
00c6131c5e30   telegraf:1.5   "/entrypoint.sh"         11 weeks ago   Up 11 weeks          my_telegraf  0 B (virtual 209.5 MB)
```

- size:   信息显示了用于每个容器的可写层的(磁盘上)数据量
- virtual size:  是容器和可写层使用的只读映像数据所使用的磁盘空间总量

### 过滤容器(-f)

过滤选项的 __-f__ 或者 __--filter__ 所支持的参数格式为 __key=value__ 格式的数据，如果你有多个过滤条件，则需要输入多个 __-f__ 或者  __--filter__ 例如：--filter  foo=bar   --filter  bif=baz

过滤条件支持的参数如下表所示

| 参数                  | 描述                                                         |
| --------------------- | ------------------------------------------------------------ |
| `id`                  | 容器的id                                                     |
| `name`                | 容器的名称                                                   |
| `label`               | 表示键或键值对的任意字符串。表示为<key>或<key>=<value>       |
| `exited`              | 容器退出码   该选项只对  -a或者(--all)选项有用               |
| `status`              | 容器状态：状态包括`created`（已创建）, `restarting（正在重启）`, `running(正在运行)`, `removing(正在删除)`, `paused(暂停)`, `exited(已退出)`, `dead(待理解，后面补充)` |
| `ancestor`            | 筛选共享的容器. 筛选参数格式为 `<image-name>[:<tag>]`, `<image id>`, or `<image@digest>` |
| `before` or `since`   | 筛选在给定容器ID或名称之前或之后创建的容器                   |
| `volume`              | 过滤已挂载给定卷或绑定挂载的运行容器                         |
| `network`             | 筛选运行连接到指定网络的容器                                 |
| `publish` or `expose` | 筛选端口. 筛选参数格式为 `<port>[/<proto>]` or `<startport-endport>/[<proto>]` |
| `health`              | 筛选容器的健康状况. 健康状况包括 `starting(已经开始的容器)`, `healthy(健康)`, `unhealthy(不健康)` or `none(没有)`. |
| `isolation`           | 只有Windows守护进程. One of `default`, `process`, or `hyperv`. |
| `is-task`             | Filters containers that are a “task” for a service. Boolean option (`true` or `false`) |



#### label

根据是否只存在一个标签或是否存在一个标签和一个值来匹配容器

标签label 是在dockerfile中用于添加元数据到镜像中的，并且label是以键值对的形式出现的

匹配是否存在color标签

```
$ docker ps --filter "label=color"
```


#### name

根据容器名称筛选

筛选所有的容器名称为redis的容器

```
docker  ps  -a  --filter name=redis
```

#### exited

根据容器退出码筛选容器，例如筛选成功退出的容器

```
docker  ps  -a  --filter exited=0
```

筛选容器退出码为 137的容器

```
docker  ps  -a --filter  exited=137
```

> 137退出状态的容器是由于下面的两种情况产生
>
> - 手动杀死容器的初始化进程
> - docker  kill  指令杀死容器
> - docker的守护进行重启杀死了正在运行的容器

#### status

根据容器的状态筛选容器,例如筛选正在运行的容器

```
docker   ps  --filter  status=running
```

> 容器的状态有下面几种:
>
> - created   已经创建
> - restarting   正在重启
> - running      正在运行
> - removing    正在删除
> - paused      暂停
> - exited         已退出
> - dead          已死亡

#### ancestor

根据镜像名称进行筛选

> - image    镜像名称
> - image:tag    指定版本的镜像
> - image:tag@digest    执行镜像版本和签名
> - short-id     镜像Id
> - full-id          完整的镜像id

根据镜像名称过滤容器

```
docker  ps  -a   --filter  ancestor=redis
```

根据指定镜像版本过滤容器

```
docker  ps  -a  --filter  ancestor=redis:4.0
```

根据镜像id筛选容器

```
docker  ps  -a  --filter ancestor=831b3dfbefc4
```

#### Create time

##### before

筛选指定容器id或容器名称创建之前的容器

筛选在redis4容器创建之前的所有容器

```
docker ps -a -f before=redis4
```

##### since

筛选指定容器名称或者容器id创建之后的容器

筛选在kibana容器创建之后的所有容器

```
docker  ps  -a -f since=kibana
```

#### volume

筛选挂载在特定文件路径下的容器

```
$ docker ps --filter volume=remote-volume --format "table {{.ID}}\t{{.Mounts}}"

CONTAINER ID        MOUNTS
9c3527ed70ce        remote-volume

$ docker ps --filter volume=/data --format "table {{.ID}}\t{{.Mounts}}"

CONTAINER ID        MOUNTS
9c3527ed70ce        remote-volume
```

#### network

筛选指定网络名称或者网络id的容器

下面的筛选器匹配所有名称包含net1的连接到网络的容器

```
$ docker run -d --net=net1 --name=test1 ubuntu top
$ docker run -d --net=net2 --name=test2 ubuntu top

$ docker ps --filter network=net1

CONTAINER ID        IMAGE       COMMAND       CREATED             STATUS              PORTS               NAMES
9d4893ed80fe        ubuntu      "top"         10 minutes ago      Up 10 minutes                           test1
```

网络过滤器同时匹配网络的名称和id。下面的例子显示了所有连接到net1网络的容器，使用网络id作为过滤器

```
$ docker network inspect --format "{{.ID}}" net1

8c0b4110ae930dbe26b258de9bc34a03f98056ed6f27f991d32919bfe401d7c5

$ docker ps --filter network=8c0b4110ae930dbe26b258de9bc34a03f98056ed6f27f991d32919bfe401d7c5

CONTAINER ID        IMAGE       COMMAND       CREATED             STATUS              PORTS               NAMES
9d4893ed80fe        ubuntu      "top"         10 minutes ago      Up 10 minutes       
```

#### publish and expose

筛选器显示带有给定端口号、端口范围和/或协议的发布或公开端口的容器。如果不指定，默认协议为tcp

匹配所有发布端口为80的容器:

```
$ docker ps --filter publish=80

CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS              PORTS                   NAMES
fc7e477723b7        busybox             "top"               About a minute ago   Up About a minute   0.0.0.0:32768->80/tcp   admiring_roentgen
```

匹配所有暴露TCP端口在8000-8080范围内的容器:

```
$ docker ps --filter expose=8000-8080/tcp

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
9833437217a5        busybox             "top"               21 seconds ago      Up 19 seconds       8080/tcp            dreamy_mccarthy
```

匹配所有暴露UDP端口80的容器:

```
$ docker ps --filter publish=80/udp
```

### 格式化打印输出(--format)

Go模板的有效占位符如下:

| 占位符        | 描述                                               |
| ------------- | -------------------------------------------------- |
| `.ID`         | 容器id                                             |
| `.Image`      | 镜像Id                                             |
| `.Command`    | 容器执行命令                                       |
| `.CreatedAt`  | 容器在什么创建的                                   |
| `.RunningFor` | 容器启动后运行的时间                               |
| `.Ports`      | 映射端口                                           |
| `.State`      | 容器状态                                           |
| `.Status`     | 包含有关持续时间和运行状况状态的详细信息的容器状态 |
| `.Size`       | 容器所用的磁盘空间                                 |
| `.Names`      | 容器名称                                           |
| `.Labels`     | 所有的容器标签                                     |
| `.Label`      | 特定标签                                           |
| `.Mounts`     | 容器卷的挂载目录                                   |
| `.Networks`   | 容器连接的网络名称                                 |

当使用  --format  操作时，若在参数中添加  __table__  字符，会显示出表头信息,反之则不会显示

不显示表头

```
$ docker ps --format "{{.ID}}: {{.Command}}"

a87ecb4f327c: /bin/sh -c #(nop) MA
01946d9d34d8: /bin/sh -c #(nop) MA
c1d3b0166030: /bin/sh -c yum -y up
41d50ecd2f57: /bin/sh -c #(nop) MA
```

显示表头

```
$ docker ps --format "table {{.ID}}\t{{.Labels}}"

CONTAINER ID        LABELS
a87ecb4f327c        com.docker.swarm.node=ubuntu,com.docker.swarm.storage=ssd
01946d9d34d8
c1d3b0166030        com.docker.swarm.node=debian,com.docker.swarm.cpu=6
41d50ecd2f57        com.docker.swarm.node=fedora,com.docker.swarm.cpu=3,com.docker.swarm.storage=ssd
```