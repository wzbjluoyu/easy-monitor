# docker network 命令

## docker network

### 描述

管理docker网络

### 用法

> docker  network  command

### 扩展描述

管理网络。您可以使用子命令来创建、检查、列出、删除、删除、连接和断开网络

## docker  network  create

### 描述

创建docker网络

### 用法

> docker  network  create [Options]  networkName

### 扩展描述

创建一个新的网络。网络类型可以接受 __bridge__  或 __overlay__ ，这是内置的网络驱动程序。如果您已经安装了第三方或您自己的自定义网络驱动程序，您也可以在这里指定该驱动程序。如果您没有指定  __--driver__ 选项，该命令将自动为您创建一个__bridge__ 类型的网络。当你安装Docker Engine时，它会自动创建一个__bridge__ 类型的网络。这个网络对应网卡名称为 __docker0__ 的网络，当你用docker运行启动一个新的容器时，它会自动连接到这个网络。您不能删除这个默认网桥网络，但是可以使用network create命令创建新的网桥网络。

```
docker network create -d bridge my-bridge-network
```

__bridge__网络安装在单个__docker engine__上的隔离网络。如果你想要创建一个跨多个__docker engine__ 主机的网络，你必须创建一个__overlay__网络。与__birdge__网络不同的是，__overlay__网络需要一些预先存在的条件。这些条件是:

- 对键值存储的访问。引擎支持Consul, Etcd和Zookeeper(分布式存储)键值存储。
- 连接到键值存储的主机集群。
- 集群中每个主机上正确配置的Engine守护进程。

支持overlay网络的docker 选项有:

- --cluster-store
- --cluster-store-opt
- --cluster-advertise

要阅读关于这些选项以及如何配置它们的更多信息，请参考[多主机网络入门](https://docs.docker.com/network/network-tutorial-overlay/)

虽然不是必需的，但安装Docker Swarm来管理组成你的网络的集群是一个好主意。Swarm提供了复杂的发现和服务器管理工具，可以帮助您实现

一旦你准备好了覆盖网络的先决条件，你只需在集群中选择一个Docker主机，并发出以下命令来创建网络:

```
$ docker network create -d overlay my-multihost-network
```

网络名称不能重复。Docker守护进程会识别命名冲突

#### __overlay__网络的局限性

您应该使用/24块(默认值)创建覆盖网络，当您使用默认的基于VIP(虚拟网络IP地址)的端点模式创建网络时，它将您的IP地址限制为256个。这个建议解决了群模式的局限性。如果需要的IP地址超过256个，请不要增加IP块大小。您可以将dnsrr端点模式与外部负载均衡器一起使用，或者使用多个较小的覆盖网络。有关不同端点模式的更多信息，请参考配置服务发现。

### Options

| 选项名称, 简写    | 默认值   | 描述                             |
| ----------------- | -------- | -------------------------------- |
| `--attachable`    |          | 启用手动连接容器                 |
| `--aux-address`   |          | 网络驱动使用的辅助IPV4或IPv6地址 |
| `--config-from`   |          | 要从中复制配置的网络             |
| `--config-only`   |          | 创建仅配置网络                   |
| `--driver` , `-d` | `bridge` | 网络驱动管理类型                 |
| `--gateway`       |          | IPv4或IPv6主子网网关             |
| `--ingress`       |          | 创建集群路由网格网络             |
| `--internal`      |          | 限制外部访问网络                 |
| `--ip-range`      |          | 在子网范围内分配容器ip           |
| `--ipam-driver`   |          | IP地址管理驱动程序               |
| `--ipam-opt`      |          | 设置IPAM驱动程序特定选项         |
| `--ipv6`          |          | 启动IPV6网络                     |
| `--label`         |          | 设置网络元数据                   |
| `--opt` , `-o`    |          | 设置驱动程序特定选项             |
| `--scope`         |          | 控制网络的范围                   |
| `--subnet`        |          | CIDR格式的子网，表示一个网段     |

### 示例

#### 连接到容器

启动容器时，使用  __--network__ 选项将容器加入到网络__mynet__中

```
$ docker run -itd --network=mynet busybox
```

如果您想在容器已经运行之后将容器添加到网络中，请使用  __docker network connect__ 子命令。

可以将多个容器连接到同一个网络中。连接后，容器只能使用另一个容器的IP地址或名称进行通信。对于支持多主机连接的__overlay__网络或自定义插件，连接到同一个多主机网络但从不同引擎启动的容器也可以以这种方式通信。

可以使用__docker network disconnect__命令断开容器与网络的连接。

#### 指定高级选项

在创建网络时，默认会为该网络创建一个不重叠的子网。该子网不是现有网络的再划分。它纯粹是为了ip寻址目的。您可以覆盖这个默认值，并直接使用  __--subnet__  选项指定子网值。在网桥网络中只能创建一个子网:

```
$ docker network create --driver=bridge --subnet=192.168.0.0/16 br0
```

此外，还可以指定  __--gateway__  __--ip-range__  和__--aux-address__选项。

```
$ docker network create \
  --driver=bridge \
  --subnet=172.28.0.0/16 \
  --ip-range=172.28.5.0/24 \
  --gateway=172.28.5.254 \
  br0
```

如果忽略 __--gateway__标志，引擎会从首选池中为您选择一个。对于__overlay__网络和支持它的网络驱动程序插件，您可以创建多个子网。这个例子使用两个/25子网掩码来遵循当前的指导，即在一个覆盖网络中不超过256个ip。每个子网都有126个可用地址。

```
$ docker network create -d overlay \
  --subnet=192.168.10.0/25 \
  --subnet=192.168.20.0/25 \
  --gateway=192.168.10.100 \
  --gateway=192.168.20.100 \
  --aux-address="my-router=192.168.10.5" --aux-address="my-switch=192.168.10.6" \
  --aux-address="my-printer=192.168.20.5" --aux-address="my-nas=192.168.20.6" \
  my-multihost-network
```

#### bridge网络驱动选项

当创建自定义网络时，默认的网络驱动程序(例如桥接程序)有额外的选项可以通过。以下是这些选项和dockerO桥接所使用的等效docker守护进程标志:

| 选项                                             | 等效        | 描述                            |
| ------------------------------------------------ | ----------- | ------------------------------- |
| `com.docker.network.bridge.name`                 | -           | 创建birdge网络时所用的网络名称  |
| `com.docker.network.bridge.enable_ip_masquerade` | `--ip-masq` | 开启ip伪装                      |
| `com.docker.network.bridge.enable_icc`           | `--icc`     | 启用/禁用容器间连接             |
| `com.docker.network.bridge.host_binding_ipv4`    | `--ip`      | 绑定容器端口时的默认IP          |
| `com.docker.network.driver.mtu`                  | `--mtu`     | 设置容器的网络MTU(最大传输单元) |
| `com.docker.network.container_interface_prefix`  | -           | 设置容器接口的自定义前缀        |

下面的参数可以被传递到任何网络驱动程序的__docker network create__中

| 选项         | 等效           | 描述                     |
| ------------ | -------------- | ------------------------ |
| `--gateway`  | -              | 设置IPV4或者IPV6的主子网 |
| `--ip-range` | `--fixed-cidr` | 从一个范围内分配ip       |
| `--internal` | -              | 限制外部访问网络         |
| `--ipv6`     | `--ipv6`       | 启用IPv6网络             |
| `--subnet`   | `--bip`        | 子网的网络               |

例如，让我们使用  __-o__或  __--opt__选项来指定发布端口时的IP地址绑定:

```
$ docker network create \
    -o "com.docker.network.bridge.host_binding_ipv4"="172.19.0.1" \
    simple-network
```

#### 网络内部模式

默认情况下，当你连接一个容器到一个__overlay__网络时，Docker也会连接一个桥接网络来提供外部连接。如果想创建一个外部隔离的覆盖网络，可以指定  __internal__选项。

#### 网络入口模式

您可以创建用于提供群集中的路由网的网络。可以在创建网络时指定  __--ingress__。一次只能创建一个入网。只有没有业务依赖网络时，才可以移除网络。创建覆盖网络时可用的任何选项在创建入口网络时也可用，除了 __--attach__选项。

```
$ docker network create -d overlay \
  --subnet=10.11.0.0/16 \
  --ingress \
  --opt com.docker.network.driver.mtu=9216 \
  --opt encrypted=true \
  my-ingress-network
```

## docker network  ls

### 描述

列举网络列表

### 用法

> docker   network   ls  [Options]

### 扩展描述

列举docker 守护进程维护的所有网络列表，包括跨集群多个主机的网络

### Options

| 选项名称, 简写    | 默认值 | 描述                                                      |
| ----------------- | ------ | --------------------------------------------------------- |
| `--filter` , `-f` |        | 提供过滤参数，过滤满足条件的网络，例如 ( 'driver=bridge') |
| `--format`        |        | 使用Go模板格式化打印输出结果                              |
| `--no-trunc`      |        | 不截断输出                                                |
| `--quiet` , `-q`  |        | 只显示网络的Id值                                          |

### 示例

#### 例举所有网络列表(ls)

```
$ sudo docker network ls
NETWORK ID          NAME                DRIVER          SCOPE
7fca4eb8c647        bridge              bridge          local
9f904ee27bf5        none                null            local
cf03ee007fb4        host                host            local
78b03ee04fc4        multi-host          overlay         swarm
```

使用选项  __--no-trunc__ 显示出完整的网络id值

```
$ docker network ls --no-trunc
NETWORK ID                                                         NAME                DRIVER           SCOPE
18a2866682b85619a026c81b98a5e375bd33e1b0936a26cc497c283d27bae9b3   none                null             local
c288470c46f6c8949c5f7e5099b5b7947b07eabe8d9a27d79a9cbf111adcbf47   host                host             local
7b369448dccbf865d397c8d2be0cda7cf7edc6b0945f77d2529912ae917a0185   bridge              bridge           local
95e74588f40db048e86320c6526440c504650a1ff3e9f7d60a497c4d2163e5bd   foo                 bridge           local
63d1ff1f77b07ca51070a8c227e962238358bd310bde1529cf62e6c307ade161   dev                 bridge           local
```

#### 过滤(--filter,-f)

过滤选项 __(--filter,-f)__ 的参数采用 __key=value__ (键值对)的形式.如果有多个过滤参数，则输入多个过滤选项并选项后跟输入参数即可。例如:  -f  type=custom  -f  type=buildtin  

过滤时可支持的参数如下:

- driver     网络驱动类型   bridge  和overlay
- id     网络的id值
- label   网络元数据
- name    网络名称
- scope    网络的作用范围   swarm(集群)  |  global(全局)  |  local(本地)
- type       网络类型      custom  |  builtin

##### driver

根据网络驱动类型过滤网络

网络驱动类型有 bridge   、overlay 、none  三种类型

```
$ docker network ls --filter driver=bridge
NETWORK ID          NAME                DRIVER            SCOPE
db9db329f835        test1               bridge            local
f6e212da9dfd        test2               bridge            local
```

##### id

通过网络id值筛选网络类型

id值可用简写的id值，也可用完整的id值

下面是使用完整的id值

```
$ docker network ls --filter id=63d1ff1f77b07ca51070a8c227e962238358bd310bde1529cf62e6c307ade161
NETWORK ID          NAME                DRIVER           SCOPE
63d1ff1f77b0        dev                 bridge           local
```

使用简写的id值

```
$ docker network ls --filter id=95e74588f40d
NETWORK ID          NAME                DRIVER          SCOPE
95e74588f40d        foo                 bridge          local

$ docker network ls --filter id=95e
NETWORK ID          NAME                DRIVER          SCOPE
95e74588f40d        foo                 bridge          local
```

:rocket:  小提示：从上面的例子可知根据id匹配是使用模糊查询的方式进行的   例如:  id   like   id值*   模糊匹配

##### label

根据网络的元数据的键，或者元数据的键=值来过滤网络

使用元数据的键来过滤网络

```
$ docker network ls -f "label=usage"
NETWORK ID          NAME                DRIVER         SCOPE
db9db329f835        test1               bridge         local
f6e212da9dfd        test2               bridge         local
```

使用元数据的键=值来过滤网络

```
$ docker network ls -f "label=usage=prod"
NETWORK ID          NAME                DRIVER        SCOPE
f6e212da9dfd        test2               bridge        local
```

##### name

根据网络的名称模糊匹配网络

使用完整网络名称过滤网络

```
$ docker network ls --filter name=foobar
NETWORK ID          NAME                DRIVER       SCOPE
06e7eef0a170        foobar              bridge       local
```

使用网络名称的部分字符模糊匹配网络

```
$ docker network ls --filter name=foo
NETWORK ID          NAME                DRIVER       SCOPE
95e74588f40d        foo                 bridge       local
06e7eef0a170        foobar              bridge       local
```

##### scope

根据网络的作用范围过滤网络

网络的作用范围可选择的值有   swarm、 global、local

```
$ docker network ls --filter scope=swarm
NETWORK ID          NAME                DRIVER              SCOPE
xbtm0v4f1lfh        ingress             overlay             swarm
ic6r88twuu92        swarmnet            overlay             swarm
```

##### type

类型筛选器支持两个值  custom builtin ,由用户创建的网络为custom类型，builtin为docker内置的网络类型

匹配用户自定义的网络类型

```
$ docker network ls --filter type=custom
NETWORK ID          NAME                DRIVER       SCOPE
95e74588f40d        foo                 bridge       local
63d1ff1f77b0        dev                 bridge       local
```

#### 格式化打印(--format)

格式化选项(__--format__)使用Go模板美化打印网络输出。

支持的Go模板占位符有:

| 占位符       | 描述                                              |
| ------------ | ------------------------------------------------- |
| `.ID`        | 网络id                                            |
| `.Name`      | 网络名称                                          |
| `.Driver`    | 网络驱动类型                                      |
| `.Scope`     | 网络作用范围                                      |
| `.IPv6`      | 网络中是否启用IPv6                                |
| `.Internal`  | 是否为内部网络                                    |
| `.Labels`    | 分配给网络的所有标签                              |
| `.Label`     | 网络的元数据，例如:{{ .Label "project.version" }} |
| `.CreatedAt` | 创建网络的时间                                    |

当使用  __--format__ 选项时，添加Go模板参数时，若添加table则会打印出所对应的表头，如果不加table则不会打印出表头

格式化打印出id、网络驱动类型，并且不添加表头

```
$ docker network ls --format "{{.ID}}: {{.Driver}}"
afaaab448eb2: bridge
d1584f8dc718: host
391df270dc66: null
```

添加表头

```
$ docker network ls --format "table  {{.ID}}: {{.Driver}}"
ID  		: Driver 
afaaab448eb2: bridge
d1584f8dc718: host
391df270dc66: null
```

