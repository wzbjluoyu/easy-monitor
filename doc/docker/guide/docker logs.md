# docker   logs  命令

## 描述

获取一个容器的日志

## 用法

> docker   logs   [Options]  container

## Options

| 选项名称, 简写        | 默认  | 描述                                                         |
| --------------------- | ----- | ------------------------------------------------------------ |
| `--details`           |       | 给日志展示额外的信息                                         |
| `--follow` , `-f`     |       | 跟踪日志输出                                                 |
| `--since`             |       | 展示从指定时间之后的日志信息                                 |
| `--tail` , `-n`       | `all` | 从日志末尾出开始显示多少条                                   |
| `--timestamps` , `-t` |       | 显示时间戳                                                   |
| `--until`             |       | 显示在时间戳(例如2013-01-02T13:23:37z)或相对(例如42m为42分钟)之前的日志 |

## 示例

检索日志直到特定的时间点

```
$ docker run --name test -d busybox sh -c "while true; do $(echo date); sleep 1; done"
$ date
Tue 14 Nov 2017 16:40:00 CET
$ docker logs -f --until=2s test
Tue 14 Nov 2017 16:40:00 CET
Tue 14 Nov 2017 16:40:01 CET
Tue 14 Nov 2017 16:40:02 CET
```

