# docker  create  指令

## 描述

创建一个容器

## 用法

> docker    create  [Options]   image   [command]  

## 扩展描述

__docker  create__ 命令是指定镜像创建一个容器，并且可给该容器提供运行指令。创建完成后会在控制台中将容器id打印出来。这类型于  __docker  run  -d__ 指令，只是容器并没有运行启动。 可以使用  __docker  start  容器id__ 命令启动容器

当你希望提前设置容器配置以便在需要时可以启动时，这非常有用。

请查看[docker  run](https://docs.docker.com/engine/reference/run/)命令，已获取更多细节

## Options

| 选项名称, 简写            | 默认值    | 描述                                                         |
| ------------------------- | --------- | ------------------------------------------------------------ |
| `--add-host`              |           | 给docker容器中的/etc/hosts文件添加主机映射  添加格式为(host:ip) |
| `--attach` , `-a`         |           | 连接容器的输入输出流                                         |
| `--blkio-weight`          |           | 容器默认磁盘IO的加权值，有效值范围为10-100                   |
| `--blkio-weight-device`   |           | 针对特定设备的IO加权控制。其格式为DEVICE_NAME:WEIGHT         |
| `--cap-add`               |           | 添加当前容器可用的Linux功能权限                              |
| `--cap-drop`              |           | 删除当前容器可用的Linux功能权限                              |
| `--cgroup-parent`         |           | 设置用于容器的默认cgroup父级                                 |
| `--cgroupns`              |           | 该选项有待进一步探索，但查询资料得知，该选项是用于划分权限组，不同的权限组可对CPU、内存等硬件资源具有不同的访问等级 |
| `--cidfile`               |           | 将容器id写入文件                                             |
| `--cpu-period`            |           | 限制CPU的调度周期                                            |
| `--cpu-quota`             |           | 限制CPU的调度比率                                            |
| `--cpu-rt-period`         |           | 限制CPU的调度周期  单位为微秒                                |
| `--cpu-rt-runtime`        |           | 限制CPU的实时运行时间  单位为微秒                            |
| `--cpu-shares` , `-c`     |           | 使用CPU所占的权重比例                                        |
| `--cpus`                  |           | 使用的CPU核心数量                                            |
| `--cpuset-cpus`           |           | 选择允许执行的CPU,可选择(0-3,0,1)具有四核CPU中，0号,1号CPU可执行 |
| `--cpuset-mems`           |           | 允许在上执行的内存节点（MEMs） (0-3, 0,1)                    |
| `--device`                |           | 为容器添加主机设备                                           |
| `--device-cgroup-rule`    |           | 向cgroup允许的设备列表中添加一条规则                         |
| `--device-read-bps`       |           | 限制从设备读取速率(每秒字节)                                 |
| `--device-read-iops`      |           | 限制从设备读取速率(每秒10次)                                 |
| `--device-write-bps`      |           | 限制设备的写速率(每秒字节数)                                 |
| `--device-write-iops`     |           | 限制设备的写速率(每秒10次)                                   |
| `--disable-content-trust` | `true`    | 是否跳过容器校验                                             |
| `--dns`                   |           | 设置自定义的域名解析服务器                                   |
| `--dns-opt`               |           | 设置DNS服务选项                                              |
| `--dns-option`            |           | 设置DNS服务选项                                              |
| `--dns-search`            |           | 设置域名解析的搜索范围                                       |
| `--domainname`            |           | 设置容器的NIS域名                                            |
| `--entrypoint`            |           | 覆盖镜像的默认ENTRYPOINT                                     |
| `--env` , `-e`            |           | 设置环境变量                                                 |
| `--env-file`              |           | 读入一个环境变量文件                                         |
| `--expose`                |           | 公开一个端口或一个端口范围                                   |
| `--gpus`                  |           | 设置要添加到容器中的GPU设备                                  |
| `--group-add`             |           | 添加要加入的其他组                                           |
| `--health-cmd`            |           | 运行健康状态检查命令                                         |
| `--health-interval`       |           | 设置状态检查命令的时间间隔                                   |
| `--health-retries`        |           | 连续失败需要报告不健康                                       |
| `--health-start-period`   |           | 启动运行状况重试倒计时(ms\| s \| m \| h)(默认Os)之前容器初始化的开始时间 |
| `--health-timeout`        |           | 允许一个检查运行的最大时间(ms \| s \| m \| h)(默认0s)        |
| `--help`                  |           | 打印使用方法                                                 |
| `--hostname` , `-h`       |           | 设置容器的主机名称                                           |
| `--init`                  |           | 在容器中运行init来转发信号和获取进程                         |
| `--interactive` , `-i`    |           | 保持连接到容器的STDIN标准输入输出流                          |
| `--ip`                    |           | 设置ip地址                                                   |
| `--ip6`                   |           | 设置ipv6的ip地址                                             |
| `--ipc`                   |           | 使用IPC模式                                                  |
| `--isolation`             |           | 容器隔离技术                                                 |
| `--kernel-memory`         |           | 内核内存限制                                                 |
| `--label` , `-l`          |           | 在容器上设置元数据                                           |
| `--label-file`            |           | 读取元数据文件到容器                                         |
| `--link`                  |           | 添加到另一个容器的链接                                       |
| `--link-local-ip`         |           | Container IPv4/IPv6 link-local addresses                     |
| `--log-driver`            |           | 记录容器的驱动程序                                           |
| `--log-opt`               |           | 设置日志驱动选项                                             |
| `--mac-address`           |           | 设置容器的MAC地址                                            |
| `--memory` , `-m`         |           | 限制容器使用的内存大小                                       |
| `--memory-reservation`    |           | Memory soft limit                                            |
| `--memory-swap`           |           | 设置容器的虚拟内存交换空间的使用大小，若指定为-1，则无限制   |
| `--memory-swappiness`     | `-1`      | 调整容器使用虚拟内存交换空间的交换比例(0 到 100)直接         |
| `--mount`                 |           | 将文件系统挂载到容器中                                       |
| `--name`                  |           | 设置容器的名称                                               |
| `--net`                   |           | 设置容器连接的网络                                           |
| `--net-alias`             |           | 设置容器连接的网络的别名                                     |
| `--network`               |           | 设置容器连接的网络                                           |
| `--network-alias`         |           | 设置容器连接的网络的别名                                     |
| `--no-healthcheck`        |           | 禁用任何容器指定的HEALTHCHECK                                |
| `--oom-kill-disable`      |           | 设置是否阻止 OOM killer杀死容器，默认为false                 |
| `--oom-score-adj`         |           | 调整容器被 OOM killer 杀死的优先级，范围是[-1000, 1000]，默认为 0 |
| `--pid`                   |           | 设置容器的pid                                                |
| `--pids-limit`            |           | 调整容器的pid数量，设置为-1表示为无限制                      |
| `--platform`              |           | 如果服务器具有多平台功能，则设置平台                         |
| `--privileged`            |           | 给这个容器扩展权限                                           |
| `--publish` , `-p`        |           | 将容器的端口于宿主机的端口做映射                             |
| `--publish-all` , `-P`    |           | 将容器所有的端口于宿主机的端口进行随机映射                   |
| `--pull`                  | `missing` | 在创建之前拉取镜像 (always  \|  missing \|  never)           |
| `--read-only`             |           | 将容器的根文件系统挂载为只读                                 |
| `--restart`               | `no`      | 在容器退出时指定策略进行重新启动                             |
| `--rm`                    |           | 当容器退出时自动删除容器                                     |
| `--runtime`               |           | 运行时用于此容器                                             |
| `--security-opt`          |           | 设置安全选项                                                 |
| `--shm-size`              |           | 设置/dev/shm所占用的磁盘空间                                 |
| `--stop-signal`           | `SIGTERM` | 设置容器停止的信号                                           |
| `--stop-timeout`          |           | 停止容器的超时时间                                           |
| `--storage-opt`           |           | 设置容器的存储驱动程序选项                                   |
| `--sysctl`                |           | 设置容器的系统选项                                           |
| `--tmpfs`                 |           | 挂载tmpfs目录                                                |
| `--tty` , `-t`            |           | 分配一个pseudo-TTY                                           |
| `--ulimit`                |           | Ulimit options                                               |
| `--user` , `-u`           |           | 设置容器的用户名称或者id  格式为(<name \| uid>[:<group \| gid>]) |
| `--userns`                |           | 设置容器的用户命名空间                                       |
| `--uts`                   |           | 设置容器使用的UTS命名空间                                    |
| `--volume` , `-v`         |           | 绑定挂载一个容器卷                                           |
| `--volume-driver`         |           | 容器的可选卷驱动程序                                         |
| `--volumes-from`          |           | 从指定的容器挂载卷                                           |
| `--workdir` , `-w`        |           | 设置容器内的工作目录                                         |



## 示例

### 创建并开始一个容器

```
$ docker create -t -i fedora bash

6d8af538ec541dd581ebc2a24153a28329acb5268abe5ef868c1f1a261221752

$ docker start -a -i 6d8af538ec5

bash-4.2#
```

### 初始化容器卷

从v1.4.0开始，容器卷在docker创建阶段初始化(也就是说，docker也运行)。例如，这允许你创建一个数据卷容器，然后从另一个容器使用它:

```
$ docker create -v /data --name data ubuntu

240633dfbb98128fa77473d3d9018f6123b99c454b3251427ae190a7d951ad57

$ docker run --rm --volumes-from data ubuntu ls -la /data

total 8
drwxr-xr-x  2 root root 4096 Dec  5 04:10 .
drwxr-xr-x 48 root root 4096 Dec  5 04:11 ..
```

类似地，创建一个主机目录绑定挂载卷容器，然后可以在后续容器中使用:

```
$ docker create -v /home/docker:/docker --name docker ubuntu

9aa88c08f319cd1e4515c3c46b0de7cc9aa75e878357b1e96f91e2c773029f03

$ docker run --rm --volumes-from docker ubuntu ls -la /docker

total 20
drwxr-sr-x  5 1000 staff  180 Dec  5 04:00 .
drwxr-xr-x 48 root root  4096 Dec  5 04:13 ..
-rw-rw-r--  1 1000 staff 3833 Dec  5 04:01 .ash_history
-rw-r--r--  1 1000 staff  446 Nov 28 11:51 .ashrc
-rw-r--r--  1 1000 staff   25 Dec  5 04:00 .gitconfig
drwxr-sr-x  3 1000 staff   60 Dec  1 03:28 .local
-rw-r--r--  1 1000 staff  920 Nov 28 11:51 .profile
drwx--S---  2 1000 staff  460 Dec  5 00:51 .ssh
drwxr-xr-x 32 1000 staff 1140 Dec  5 04:01 docker
```

设置每个容器的存储驱动程序选项

```
$ docker create -it --storage-opt size=120G fedora /bin/bash
```

这个(大小)将允许在创建时将容器rootfs大小设置为120G。这个选项只适用于devicemapper, btrfs, overlay2 windowsfilter和zfs图形驱动程序。对于devicemapper, btrfs, windowsfilter和zfs图形驱动，用户不能传递小于默认BaseFS大小，对于overlay2存储驱动，大小选项只有当支持的fs是xfs和pquota挂载选项是可用的。在这些条件下，用户可以通过任何小于备份文件系统大小的大小。

### 为容器指定隔离技术(--isolation)

在Windows上运行Docker容器时，此选项非常有用。——isolation=<value>选项设置容器的隔离技术。在Linux上，唯一支持的是使用Linux名称空间的默认选项。在Microsoft Windows上，您可以指定这些值

| Value                                                      | 描述                                                         |
| ---------------------------------------------------------- | ------------------------------------------------------------ |
| `default`                                                  | 使用Docker守护进程的__--exec-opt__指定的值如果守护进程没有指定隔离技术，Microsoft Windows使用进程作为它的默认值 |
| daemon运行在Windows服务器上，hyperv运行在Windows客户端上。 |                                                              |
| `process`                                                  | 名称空间的隔离,                                              |
| `hyperv`                                                   | Hyper-V hypervisor基于分区的隔离。                           |

指定不带值的  __--isolation__ 标志与设置  __isolation="default"__ 相同

### 动态设置容器可访问的硬件的群组规则(--device-cgroup-rule)

容器可用的设备是在创建时分配的。分配的设备都将被添加到cgroup中。允许文件和创建到容器一旦运行。当需要向运行的容器中添加新设备时，这就产生了一个问题。

解决方案之一是向容器添加一个更宽松的规则，允许它访问更广泛的设备。例如，假设我们的容器需要访问一个主设备42和任意数量的副设备(在新设备出现时添加)，将添加以下规则

```
docker create --device-cgroup-rule='c 42:* rmw' -name my-container my-image
```

然后，用户可以要求__udev__执行一个脚本，docker exec my-container mknod newDevx c42 <minor>添加所需的设备。

注意:最初的设备仍然需要显式地添加到create/run命令中