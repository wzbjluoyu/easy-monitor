# docker pull 指令

## 描述

从远程库中的拉取镜像

## 使用方法

> docker   pull   [Options]  NAME[:TAG|@DIGEST]

## 扩展描述

你创建的大多数镜像依赖与[Docker hub](https://registry.hub.docker.com/)上的基础镜像，例如基础镜像Linux操作系统等，有了这些基础镜像你才可以在镜像之上构建应用

[Docker hub](https://registry.hub.docker.com/)上包含需要已经构建好的镜像，你可以根据需要获取

从Docker hub上获取一个特定的镜像，你需要使用指令 __docker pull __ 

### 代理配置

如果你在HTTP代理服务器后面，例如在公司设置中，在打开注册表连接之前，你可能需要配置Docker守护进程的代理设置，使用HTTP代理，HTTPS代理，和NO代理环境变量。要使用systemd在主机上设置这些环境变量，请参考控制并使用[systemd配置Docker的变量配置](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)

### 并发下载

默认情况下，Docker守护进程将一次提取镜像的三层。如果您使用的是低带宽连接，这可能会导致超时问题，您可能希望通过  __--max-concurrent-downloads__  守护进程选项来降低超时。有关更多细节，请参阅[守护进程文档](https://docs.docker.com/engine/reference/commandline/dockerd/)

关于此命令的使用示例，请参阅下面的示例部分

## Options

| 名称, 缩写                | 默认值 | 描述                                     |
| ------------------------- | ------ | ---------------------------------------- |
| `--all-tags` , `-a`       |        | 从仓库中下载指定镜像，并且下载所有的版本 |
| `--disable-content-trust` | `true` | 是否跳过镜像验证，默认值为true  表示跳过 |
| `--platform`              |        | 如果服务器具有多平台功能，则设置平台     |
| `--quiet` , `-q`          |        | 拉取镜像时不显示拉取过程                 |

## 示例

#### 从Docker hub 中拉取一个最新镜像

使用 __docker  pull__ 指令拉取一个镜像，如果没有指定版本，docker会自动指定为  __:latest__ 版本，即当前镜像的最新版本

```
$ docker pull debian

Using default tag: latest       
latest: Pulling from library/debian
fdd5d7827f33: Pull complete
a3ed95caeb02: Pull complete
Digest: sha256:e7d38b3517548a1c71e41bffe9c8ae6d6d29546ce46bf62159837aad072c90aa
Status: Downloaded newer image for debian:latest
```

Docker图像可以由多层组成。在上面的例子中，图像由两层组成  __fdd5d7827f33__ 和  __a3ed95caebo2__

### 从Docker hub中拉取一个指定版本的镜像

> :notebook: 提示
>
> 如需获取镜像的版本信息，可到Dokcer hub中搜索指定镜像，即可查看所有的版本信息

```
$ docker pull ubuntu:14.04

14.04: Pulling from library/ubuntu
5a132a7e7af1: Pull complete
fd2731e4c50c: Pull complete
28a2f68d1120: Pull complete
a3ed95caeb02: Pull complete
Digest: sha256:45b23dee08af5e43a7fea6c4cf9c25ccf269ee113168c19722f87876677c5cb2
Status: Downloaded newer image for ubuntu:14.04
```

### 通过摘要信息从Docker hub中拉取指定镜像 

```
$ docker pull ubuntu@sha256:45b23dee08af5e43a7fea6c4cf9c25ccf269ee113168c19722f87876677c5cb2

sha256:45b23dee08af5e43a7fea6c4cf9c25ccf269ee113168c19722f87876677c5cb2: Pulling from library/ubuntu
5a132a7e7af1: Already exists
fd2731e4c50c: Already exists
28a2f68d1120: Already exists
a3ed95caeb02: Already exists
Digest: sha256:45b23dee08af5e43a7fea6c4cf9c25ccf269ee113168c19722f87876677c5cb2
Status: Downloaded newer image for ubuntu@sha256:45b23dee08af5e43a7fea6c4cf9c25ccf269ee113168c19722f87876677c5cb2
```

拉取完成后可使用指令 __docker  images __ 查看已经拉取完成的镜像

```
$ docker images

REPOSITORY   TAG      IMAGE ID        CREATED      SIZE
debian       jessie   f50f9524513f    5 days ago   125.1 MB
debian       latest   f50f9524513f    5 days ago   125.1 MB
```

> :notebook: 提示
>
> 使用这个功能可以及时将镜像指定到一个特定的版本。因此，Docker不会拉出包含安全更新的镜像的更新版本。如果要拉出更新后的图像，则需要相应地更改摘要。

### 拉取时指定远程仓库

默认情况下拉取镜像时是从Docker hub中拉取，但是你也可以指定一个远程仓库，从指定的远程仓库中拉取镜像，但是要注意的是指定远程仓库时，不需要添加协议标识符，例如https://baidu.com ，不添加协议标识符的写法为__baidu.com__

```
$ docker pull myregistry.local:5000/testing/test-image
```

> :warning: 注意
>
> 指定远程仓库为自定义的私有docker仓库，不是镜像加速服务器
>
> 具体搭建docker私有库可参考[Docker搭建本地私有仓库](https://blog.csdn.net/u010397369/article/details/42422243)

### 拉取指定镜像的所有版本

默认情况下，__docker pull__ 拉取镜像是拉取单个镜像，如果需要拉取该镜像的所有版本的镜像，可使用选项 __-a__ 或者(__--all-tags__)

下面这个示例是拉取  __fedora__ 镜像的全部版本

```
$ docker pull --all-tags fedora

Pulling repository fedora
ad57ef8d78d7: Download complete
105182bb5e8b: Download complete
511136ea3c5a: Download complete
73bd853d2ea5: Download complete
....

Status: Downloaded newer image for fedora
```

拉取完成后，使用docker images命令查看被拉取的图像。下面的例子显示了本地存在的所有fedora图像:happy:

```
$ docker images fedora

REPOSITORY   TAG         IMAGE ID        CREATED      SIZE
fedora       rawhide     ad57ef8d78d7    5 days ago   359.3 MB
fedora       20          105182bb5e8b    5 days ago   372.7 MB
fedora       heisenbug   105182bb5e8b    5 days ago   372.7 MB
fedora       latest      105182bb5e8b    5 days ago   372.7 MB
```

### 取消拉取

如果你在拉取过程中，突然不想要当前镜像了，可以在键盘上按下组合键位 __Ctrl  +  c__ 取消拉取

```
$ docker pull fedora

Using default tag: latest
latest: Pulling from library/fedora
a3ed95caeb02: Pulling fs layer
236608c7b546: Pulling fs layer
^C
```

> :warning: 注意
> 如果Docker引擎和Docker 守护进程与Docker拉取客户端之间的连接端口，那么拉取也会被取消掉

