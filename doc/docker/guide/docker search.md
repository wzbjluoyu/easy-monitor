# docker search 

## 概述

在Docker hub中搜索镜像

## 用法

> docker   search   [options]   term

## 扩展参数

该命令的使用示例，请参考下面的示例

| 参数名称, 简写    | 默认 | 描述                                                         |
| ----------------- | ---- | ------------------------------------------------------------ |
| `--filter` , `-f` |      | 根据所提供的条件过滤输出                                     |
| `--format`        |      | 使用Go模板美化打印搜索输出                                   |
| `--limit`         | `25` | 最大显示结果   默认最大展示25条                              |
| `--no-trunc`      |      | 不截断输出,默认情况下，如果镜像的描述信息过长只会显示部分描述信息 |

## 示例

### 根据名称搜索镜像

这个例子显示了一个包含busybox的名称的图像:

```
$ docker search busybox

NAME                             DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
busybox                          Busybox base image.                             316       [OK]
progrium/busybox                                                                 50                   [OK]
radial/busyboxplus               Full-chain, Internet enabled, busybox made...   8                    [OK]
odise/busybox-python                                                             2                    [OK]
.......此处省略22条

```

### 显示全部的镜像描述信息（--no-trunc）

 这个例子显示了一个包含'busybox，至少3颗星，并且描述在输出时显示镜像的全部描述信息

```
$ docker search --filter=stars=3 --no-trunc busybox
NAME                 DESCRIPTION                                                                               STARS     OFFICIAL   AUTOMATED
busybox              Busybox base image.                                                                       325       [OK]
progrium/busybox                                                                                               50                   [OK]
radial/busyboxplus   Full-chain, Internet enabled, busybox made from scratch. Comes in git and cURL flavors.   8    
```

### 限制搜索的返回结果数量(--limit)

将搜索结果限制在指定范围内，limit的值可选择 1到100之间的正整数字，不指定则默认为25条

> docker search   --limit=20   redis

### 过滤返回结果(--filter,-f)

将搜索结果过滤，填入的搜索条件应该是使用key=value形式的条件，例如 -f=stars=100

可支持筛选的条件如下

- stars   镜像的stars数    -f=stars=100 表示过滤starts数量大于等于100的镜像
- is-automated   镜像是否为自动构建的镜像    可选值为true和false
- is-official 镜像是否为官方构建的镜像，可选值为true和false

#### stars

下面的例子是搜索镜像名称为busybox，搜索后过滤stars数量大于等于3的结果

```
$ docker search --filter stars=3 busybox

NAME                 DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
busybox              Busybox base image.                             325       [OK]
progrium/busybox                                                     50                   [OK]
radial/busyboxplus   Full-chain, Internet enabled, busybox made...   8                    [OK]
```

#### is-automated

下面的例子是搜索镜像名称为busybox,并且为自动构建的镜像

```
$ docker search --filter is-automated=true busybox

NAME                 DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
progrium/busybox                                                     50                   [OK]
radial/busyboxplus   Full-chain, Internet enabled, busybox made...   8                    [OK]
```

#### is=official

下面的例子是搜索镜像名称为busybox,并且为官方构建的，stars数大于等于3的镜像

```
$ docker search --filter is-official=true --filter stars=3 busybox

NAME                 DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
progrium/busybox                                                     50                   [OK]
radial/busyboxplus   Full-chain, Internet enabled, busybox made...   8                    [OK]
```

### 格式化输出（--format）

使用Go模板打印出美观的输出结果
Go模板的占位符如下表所示

| 占位符         | 描述                       |
| -------------- | -------------------------- |
| `.Name`        | 镜像名称                   |
| `.Description` | 镜像描述                   |
| `.StarCount`   | 镜像stars数量              |
| `.IsOfficial`  | 如果镜像为官方镜像则为“OK” |
| `.IsAutomated` | 如果为自动构建镜像则为"OK" |

在使用--format输出时，如果你在占位符的前面添加了  table参数，会显示出了列名，如果没有添加则不会显示列名。占位符使用  __{{}}__  包裹，多个占位符之间使用 __:__  隔开

```
$ docker search --format "{{.Name}}: {{.StarCount}}" nginx

nginx: 5441
jwilder/nginx-proxy: 953
richarvey/nginx-php-fpm: 353
million12/nginx-php: 75
webdevops/php-nginx: 70
h3nrik/nginx-ldap: 35
bitnami/nginx: 23
evild/alpine-nginx: 14
million12/nginx: 9
maxexcloo/nginx: 7
```

下面的例子使用table参数展示列名

```
$ docker search --format "table {{.Name}}\t{{.IsAutomated}}\t{{.IsOfficial}}" nginx

NAME                                     AUTOMATED           OFFICIAL
nginx                                                        [OK]
jwilder/nginx-proxy                      [OK]
richarvey/nginx-php-fpm                  [OK]
jrcs/letsencrypt-nginx-proxy-companion   [OK]
million12/nginx-php                      [OK]
webdevops/php-nginx                      [OK]
```