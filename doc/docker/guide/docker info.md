# docker  info 命令

## 描述

显示整个docker系统的信息

## 用法

> docker  info  [Options]

## 扩展描述

此命令显示关于Docker安装的系统范围信息。显示的信息包括内核版本、容器数量和镜像。所显示镜像的数量是唯一镜像的数量。以不同名称标记的相同镜像只计算一次

如果指定了格式，将执行给定的模板，而不是默认格式。Go的文本/模板包描述了格式的所有细节

根据使用的存储驱动程序，还可以显示其他信息，如池名称、数据文件、元数据文件、使用的数据空间、总数据空间、使用的元数据空间和总元数据空间

数据文件是存储图像的地方，而元数据文件是存储有关这些图像的元数据的地方。当第一次运行时，Docker会从/var/lib/docker挂载的卷上的可用空间中分配一定数量的数据空间和元数据空间

## Options

| 选项名称, 简写    | 默认值 | 描述                       |
| ----------------- | ------ | -------------------------- |
| `--format` , `-f` |        | 使用给定的Go模板格式化输出 |

## 示例

### 显示输出

下面的示例显示了在Red Hat Enterprise Linux上运行的守护进程的输出，它使用devicemapper存储驱动程序。从输出中可以看到，关于devicemapper存储驱动程序的附加信息显示:

```
$ docker info
Client:
 Context:    default
 Debug Mode: false

Server:
 Containers: 14
  Running: 3
  Paused: 1
  Stopped: 10
 Images: 52
 Server Version: 1.10.3
 Storage Driver: devicemapper
  Pool Name: docker-202:2-25583803-pool
  Pool Blocksize: 65.54 kB
  Base Device Size: 10.74 GB
  Backing Filesystem: xfs
  Data file: /dev/loop0
  Metadata file: /dev/loop1
  Data Space Used: 1.68 GB
  Data Space Total: 107.4 GB
  Data Space Available: 7.548 GB
  Metadata Space Used: 2.322 MB
  Metadata Space Total: 2.147 GB
  Metadata Space Available: 2.145 GB
  Udev Sync Supported: true
  Deferred Removal Enabled: false
  Deferred Deletion Enabled: false
  Deferred Deleted Device Count: 0
  Data loop file: /var/lib/docker/devicemapper/devicemapper/data
  Metadata loop file: /var/lib/docker/devicemapper/devicemapper/metadata
  Library Version: 1.02.107-RHEL7 (2015-12-01)
 Execution Driver: native-0.2
 Logging Driver: json-file
 Plugins:
  Volume: local
  Network: null host bridge
 Kernel Version: 3.10.0-327.el7.x86_64
 Operating System: Red Hat Enterprise Linux Server 7.2 (Maipo)
 OSType: linux
 Architecture: x86_64
 CPUs: 1
 Total Memory: 991.7 MiB
 Name: ip-172-30-0-91.ec2.internal
 ID: I54V:OLXT:HVMM:TPKO:JPHQ:CQCD:JNLC:O3BZ:4ZVJ:43XJ:PFHZ:6N2S
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Username: gordontheturtle
 Registry: https://index.docker.io/v1/
 Insecure registries:
  myinsecurehost:5000
  127.0.0.0/8
```

下面是一个Ubuntu上运行的守护进程的示例输出，使用的是overlay2存储驱动程序和一个2节点群中的节点:

```
$ docker -D info
Client:
 Context:    default
 Debug Mode: true

Server:
 Containers: 14
  Running: 3
  Paused: 1
  Stopped: 10
 Images: 52
 Server Version: 1.13.0
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Native Overlay Diff: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Plugins:
  Volume: local
  Network: bridge host macvlan null overlay
 Swarm: active
  NodeID: rdjq45w1op418waxlairloqbm
  Is Manager: true
  ClusterID: te8kdyw33n36fqiz74bfjeixd
  Managers: 1
  Nodes: 2
  Orchestration:
   Task History Retention Limit: 5
  Raft:
   Snapshot Interval: 10000
   Number of Old Snapshots to Retain: 0
   Heartbeat Tick: 1
   Election Tick: 3
  Dispatcher:
   Heartbeat Period: 5 seconds
  CA Configuration:
   Expiry Duration: 3 months
  Root Rotation In Progress: false
  Node Address: 172.16.66.128 172.16.66.129
  Manager Addresses:
   172.16.66.128:2477
 Runtimes: runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 8517738ba4b82aff5662c97ca4627e7e4d03b531
 runc version: ac031b5bf1cc92239461125f4c1ffb760522bbf2
 init version: N/A (expected: v0.13.0)
 Security Options:
  apparmor
  seccomp
   Profile: default
 Kernel Version: 4.4.0-31-generic
 Operating System: Ubuntu 16.04.1 LTS
 OSType: linux
 Architecture: x86_64
 CPUs: 2
 Total Memory: 1.937 GiB
 Name: ubuntu
 ID: H52R:7ZR6:EIIA:76JG:ORIY:BVKF:GSFU:HNPG:B5MK:APSC:SZ3Q:N326
 Docker Root Dir: /var/lib/docker
 Debug Mode: true
  File Descriptors: 30
  Goroutines: 123
  System Time: 2016-11-12T17:24:37.955404361-08:00
  EventsListeners: 0
 Http Proxy: http://test:test@proxy.example.com:8080
 Https Proxy: https://test:test@proxy.example.com:8080
 No Proxy: localhost,127.0.0.1,docker-registry.somecorporation.com
 Registry: https://index.docker.io/v1/
 WARNING: No swap limit support
 Labels:
  storage=ssd
  staging=true
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Registry Mirrors:
   http://192.168.1.2/
   http://registry-mirror.example.com:5000/
 Live Restore Enabled: false
```

### 格式化输出

你也可以指定输出格式:

```
$ docker info --format '{{json .}}'

{"ID":"I54V:OLXT:HVMM:TPKO:JPHQ:CQCD:JNLC:O3BZ:4ZVJ:43XJ:PFHZ:6N2S","Containers":14, ...}
```