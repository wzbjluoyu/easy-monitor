# Docker  run reference

Docker在隔离的容器中运行进程。容器是在主机上运行的进程。主机可以是本地的，也可以是远程的。当一个操作符执行docker运行时，运行的容器进程是隔离的，因为它有自己的文件系统、自己的网络和独立于主机的独立进程树。 

本页面详细介绍了如何使用docker run命令在运行时定义容器的资源。 

## Docker run 的指令形式

```
docker run [OPTIONS] IMAGE[:TAG|@DIGEST] [COMMAND] [ARG...]
```

docker run命令必须指定一个镜像来派生容器。图像开发人员可以定义以下镜像的默认值:

- 后台运行或前台交互式运行
- 容器识别
- 网络配置
- 指定运行时容器可用的CPU和内存空间

使用docker run [OPTIONS]操作符可以添加或覆盖由开发人员设置的映像默认值。此外，操作符可以覆盖几乎所有由Docker运行时本身设置的缺省值。操作员能够覆盖镜像和Docker运行时默认值，这就是为什么run比任何其他Docker命令有更多的选项

要了解如何解释[OPTIONS]的类型，请参见选项类型

> :notebook: 笔记
>
> 根据您的Docker系统配置，您可能需要在Docker运行命令前使用sudo。为了避免在使用docker命令时使用sudo，系统管理员可以创建一个名为docker的Unix组，并向其添加用户。有关此配置的更多信息，请参考您的操作系统的Docker安装文档

## Docker 选项

### 后台运行与前台交互运行

当启动Docker容器时，你必须首先决定是在后台以“分离”模式运行容器还是在默认的前台模式运行容器

```
-d=false  后台运行模式：后台运行容器并打印出容器id
```

### 后台分离运行选项(-d)

要以分离模式启动容器，可以使用-d-true或仅使用-d选项。按照设计，在分离模式下启动的容器将在用于运行容器的根进程退出时退出，除非您还指定了--rm选项。如果对rm使用-d，则当容器退出或守护进程退出时，容器将被删除，以先发生的日期为准。

不要将service x start命令传递给分离的容器。例如，该命令试图启动nginx服务。

```
 docker run -d -p 80:80 my_image service nginx start
```

这成功地启动了容器内的nginx服务。然而，它失败了分离容器的范例，因为根进程(服务nginx start)返回，分离的容器按照设计停止。结果，nginx服务启动了，但不能使用。相反，要启动一个进程，如nginx web服务器，执行以下操作:

```
docker run -d -p 80:80 my_image nginx -g 'daemon off;'
```

要使用分离的容器进行输入/输出，请使用网络连接或共享卷。这些是必需的，因为容器不再监听运行docker的命令行。

要重新连接到分离的容器，请使用docker attach命令

### 前台交互运行

在前台模式下(未指定-d时默认)，docker run可以在容器中启动进程，并将控制台附加到进程的标准输入、输出和标准错误。它甚至可以伪装成TTY(这是大多数命令行可执行文件所期望的)和。传递信号。所有这些都是可配置的:

```
-a=[]           : Attach to `STDIN`, `STDOUT` and/or `STDERR`
-t              : Allocate a pseudo-tty
--sig-proxy=true: Proxy all received signals to the process (non-TTY mode only)
-i              : Keep STDIN open even if not attached
```

如果你没有指定-a，那么Docker将同时连接到stdout和stderr。你可以指定你想连接哪一个标准流(STDIN, STDOUT, STDERR)，如:

```
docker run -a stdin -a stdout -i -t ubuntu /bin/bash
```

对于交互式进程(如shell)，你必须同时使用-i-t来为容器进程分配tty。-i-t通常被写成-it，您将在后面的例子中看到。当客户端从管道接收标准输入时，禁止指定-t，例如:

```
$ echo test | docker run -i busybox cat
```

> :notebook:笔记
>
> 在容器中以PID 1的形式运行的进程会被Linux特别处理:它会忽略任何带有默认动作的信号。因此，进程不会在SIGINT或SIGTERM上终止，除非它被编码为这样做

### 容器名称（--name）

可以使用三种方式标识一个容器

| 标识类型              | 示例值                                                       |
| --------------------- | ------------------------------------------------------------ |
| UUID long identifier  | “f78375b1c487e03c9438c729345e54db9d20cfa2ac1fc3494b6eb60872e74778” |
| UUID short identifier | “f78375b1c487”                                               |
| Name                  | “evil_ptolemy”                                               |

UUID标识符来自Docker守护进程。如果您没有使用——name选项分配容器名称，那么守护进程将为您生成一个随机字符串名称。定义名称可以方便地为容器添加含义。如果你指定了一个名称，你可以在Docker网络中引用容器时使用它。这适用于后台和前台的Docker容器。

> :notebook: 笔记
>
> 默认桥接网络上的容器必须链接容器名称进行通信。

### 将容器写入PID文件

最后，为了帮助实现自动化，您可以让Docker将容器ID写入您选择的文件中。这类似于一些程序可能会把它们的进程ID写入一个文件(你已经看到它们作为PID文件):

```
--cidfile="": 将容器id写入文件
```

### 镜像版本:tag

虽然这不是一种严格意义上的标识容器的方法，但是您可以通过向命令中添加“imagel:tag”来指定希望运行容器的映像版本。例如docker运行ubuntu:14.04

### image[@digest]

Images using the v2 or later image format have a content-addressable identifier called a digest. As long as the input used to generate the image is unchanged, the digest value is predictable and referenceable. 

The following example runs a container from the `alpine` image with the 

sha256:9cacb71397b640eca97488cf08582ae4e4068513101088e9f96c9814bfda95e0  digest: 

```
 docker run alpine@sha256:9cacb71397b640eca97488cf08582ae4e4068513101088e9f96c9814bfda95e0 date
```

### PID 设置 [--pid]

```
--pid=""  : Set the PID (Process) Namespace mode for the container,
             'container:<name|id>': joins another container's PID namespace
             'host': use the host's PID namespace inside the container
```

默认情况下，所有容器都启用了PID命名空间。

PID命名空间提供进程分离。PID命名空间删除了系统进程的视图，允许进程id被重用，包括PID 1

在某些情况下，您希望容器共享主机的进程名称空间，基本上允许容器内的进程查看系统上的所有进程。例如，vou可以使用调试工具(如strace或edb)构建一个容器，但在调试容器内的进程时希望使用这些工具。

### 示例：在容器中运行htop

创建Dockerfile:

```
FROM alpine:latest
RUN apk add --update htop && rm -rf /var/cache/apk/*
CMD ["htop"]
```

构建这个Dockerfile的镜像和版本为myhtop

```
 docker build -t myhtop .
```

使用以下命令在容器中运行htop:

```
$ docker run -it --rm --pid=host myhtop
```

可以使用加入另一个容器的pid名称空间来调试该容器

### 示例

启动一个运行redis服务器的容器:

```
$ docker run --name my-redis -d redis
```

通过运行另一个包含strace的容器来调试redis容器:

```
$ docker run -it --pid=container:my-redis my_strace_docker_image bash
$ strace -p 1
```
### UTS命名空间设置[--uts]

```
--uts=""  : 设置容器的UTS命名空间模式，
       设置容器的UTS命名空间模式，
```

UTS名称空间用于设置在该名称空间中运行的进程可见的主机名和域。默认情况下，所有容器(包括那些带有——network-host的容器)都有自己的UTS名称空间。主机设置将导致容器使用与主机相同的UTS名称空间。注意 --hostnameh和--domainname在主机UTS模式下无效

如果您希望容器的主机名随着主机名的更改而更改，那么您可能希望与主机共享UTS名称空间。更高级的用例是将主机的主机名从容器更改

### IPC命名空间设置[--ipc]

```
--ipc="MODE"  : 设置容器的IPC模式
```

MODE的可选值如下表:

| Value                       | Description                                 |
| --------------------------- | ------------------------------------------- |
| ””                          | 使用守护进程默认的命名空间                  |
| “none”                      | 拥有私有IPC命名空间，没有挂载/dev/shm。     |
| “private”                   | 拥有私有IPC名称空间                         |
| “shareable”                 | 拥有私有IPC名称空间，可以与其他容器共享它。 |
| “container: <_name-or-ID_>" | 使用主机系统的IPC名称空间                   |
| “host”                      | 使用主机系统的IPC名称空间。                 |

如果未指定，则使用daemon default，它可以是“private””或可共享的”，这取决于守护进程的版本和配置。

IPC (POSIX/SysV IPC)命名空间提供了命名共享内存段、信号量和消息队列的分离。

共享内存段用于以内存速度加速进程间通信，而不是通过管道或网络堆栈。共享内存通常用于科学计算和金融服务行业的数据库和定制的高性能应用程序(通常是C/OpenMPI、c++ /使用boost库)。如果这些类型的应用程序被分解成多个容器，您可能需要共享容器的IPC机制，使用“shareable”模式用于主容器(即。对于其他容器，则使用"container:<donor-name-or- id >"。

### 网络设置

```
--dns=[]           : 设置容器的自定义dns服务器
--network="bridge" : 容器连接到一个网络中，连接的网络类型为“bridge"
--network-alias=[] : 为容器添加网络的别名
--add-host=""      : 添加一行到/etc/hosts (host: IP)
--mac-address=""   : 设置容器的以太网设备的MAC地址
--ip=""            : 设置容器的以太网设备的IPv4地址
--ip6=""           : 设置容器的以太网设备的IPv6地址
--link-local-ip=[] : 设置一个或多个容器的以太网设备的链路本地IPv4/IPv6地址
```

默认情况下，所有容器都启用了网络，它们可以进行任何传出连接。运营商可以完全禁用网络与docker运行——网络none，禁用所有传入和传出的网络。在这种情况下，您只能通过文件或STDIN和SтDоUт执行I/O。

发布端口和链接到其他容器只能使用默认(桥)。链接特性是一个遗留特性。你应该总是更喜欢使用Docker网络驱动程序而不是链接。

默认情况下，容器将使用与主机相同的DNS服务器，但可以使用——DNS覆盖

缺省情况下，MAC地址使用分配给容器的IP地址生成。你可以通过——MAC -address参数(format: 12:34:56:78:9a:bc)显式地设置容器的MAC地址。请注意，Docker不会检查手动指定的MAC地址是否唯一

支持的网络类型如下表

| Network                  | Description                                               |
| ------------------------ | --------------------------------------------------------- |
| **none**                 | 容器中没有网络                                            |
| **bridge** (default)     | 通过veth接口将容器连接到网桥                              |
| **host**                 | 在容器中使用主机的网络协议栈                              |
| **container**:<name\|id> | 使用另一个容器的网络协议栈，通过它的名称或id指定。        |
| **NETWORK**              | 将容器连接到用户创建的网络(使用docker network create命令) |

#### 网络: none

如果没有网络，容器将无法访问任何外部路由。容器中仍然启用了一个loopback接口(网络回环接口)，但是它没有任何到外部通信的路由

#### 网络:  bridge

通过网络设置桥接容器将使用docker的默认网络设置。在主机上设置一个网桥，通常命名为dockere，并为容器创建一对veth接口。veth对的一边将保持在连接到网桥的主机上，而对的另一边将放置在容器的名称空间中，除了loopback接口之外。将为网桥网络中的集装箱分配一个IP地址，流量将通过网桥路由到集装箱。

默认情况下，容器可以通过它们的IP地址进行通信。要通过名称进行通信，它们必须是链接的。

网络:  host

通过将网络设置为宿主，容器将共享该主机的网络堆栈，并且该主机的所有接口都对容器可用。容器的主机名将与主机系统上的主机名匹配。注意——mac-address在主机netmode下无效。即使在主机网络模式下，容器默认也有自己的UTS名称空间。因此——主机名和域名在主机网络模式下是允许的，并且只会改变容器内部的主机名和域名。与——hostname类似，-add-host、——dns、——dns-search和——dns-option选项可以在主机网络模式下使用。这些选项更新容器内的/etc/hosts或/etc/resolv.conf。主机的“/etc/hosts”和“/etc/resolv.conf”未修改

与默认桥接模式相比，主机模式提供了明显更好的网络性能，因为它使用主机的本机网络堆栈，而桥接必须通过docker守护进程进行一层虚拟化，建议运行容器。在这种模式下，它们的网络性能非常关键。例如，生产负载均衡器或高性能Web服务器。

> :notebook: 笔记
>
> --network="host"  •允许容器完全访问本地系统服务，如D-bus，因此被认为是不安全的

#### 网络: container

通过将网络设置为容器，容器将共享另一个容器的网络堆栈。另一个容器的名称必须以,--network container:<name id>的格式提供。注意，--add-host, --hostname ,--dns,--dns-search,--dns-option和,--mac-address在容器的netmode中无效，--publish,--publish-all,--expose在容器的netmode中也无效。

示例:运行一个Redis容器，Redis绑定到localhost，然后运行Redis -cli命令，通过localhost接口连接到Redis服务器

```
$ docker run -d --name redis example/redis --bind 127.0.0.1
$ # 使用redis容器的网络堆栈访问localhost
$ docker run --rm -it --network container:redis example/redis-cli -h 127.0.0.1
```

#### 用户自定义网络

您可以使用Docker网络驱动程序或外部网络驱动程序插件来创建网络。可以将多个容器连接到同一个网络中。一旦连接到用户定义的网络，这些容器就可以轻松地使用另一个容器的IP地址或名称进行通信。

对于支持多主机连接的覆盖网络或自定义插件，连接到同一个多主机网络但从不同引擎启动的容器也可以以这种方式通信

下面的示例使用内置桥接网络驱动程序创建一个网络，并在创建的网络中运行一个容器

```
$ docker network create -d bridge my-net
$ docker run --network=my-net -itd --name=container3 busybox
```

#### 管理 /etc/hosts

您的容器将在/etc/hosts中有一些代码行，它们定义了容器本身的主机名以及localhost和其他一些常见的东西。--add-host标志可以用于向/etc/hosts添加额外的行

```
 docker run -it --add-host db-static:86.75.30.9 ubuntu cat /etc/hosts

172.17.0.22     09d03f76bf2c
fe00::0         ip6-localnet
ff00::0         ip6-mcastprefix
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
127.0.0.1       localhost
::1	            localhost ip6-localhost ip6-loopback
86.75.30.9      db-static
```

如果一个容器连接到默认桥接网络并与其他容器链接，那么容器的/etc/hosts文件将使用链接的容器名称进行更新

> :notebook: 笔记
>
> 由于Docker可能实时更新容器的/etc/hosts文件，因此可能会出现容器内的进程最终读取一个空的或不完整的/etc/hosts文件的情况。在大多数情况下，重新尝试读取应该可以解决这个问题

#### 重启策略[--restart]

在Docker运行时使用--restart选项，你可以指定一个容器退出时应该或不应该重新启动的重启策略

当容器上的重启策略处于活动状态时，在docker ps中将显示为up或Restarting。使用docker事件查看重启策略是否有效也很有用

Docker支持以下重启策略:

| Policy                       | Result                                                       |
| ---------------------------- | ------------------------------------------------------------ |
| **no**                       | 当容器退出时，不要自动重启容器。这是默认值。                 |
| **on-failure**[:max-retries] | 只有当容器以非零退出状态退出时才重新启动。可选地，限制Docker守护进程尝试重新启动的次数 |
| **always**                   | 无论退出状态如何，都要重新启动容器。当您指定always时，Docker守护进程将尝试无限期地重新启动容器。容器也将始终在守护进程启动时启动，而不管容器的当前状态如何。 |
| **unless-stopped**           | 无论退出状态如何，都要重启容器，包括在daemon启动时，除非在Docker daemon停止之前容器已经进入停止状态 |

在每次重启前添加一个不断增加的延迟(从100毫秒开始，是之前延迟的两倍)，以防止服务器被淹没。这意味着守护进程将等待100 ms，然后200 ms, 400, 800, 1600，等等，直到达到故障限制，或者当您停止docker或docker rm-f容器

如果容器成功重启(容器启动并运行至少10秒)，延迟将重置为默认值100毫秒

当使用on-failure策略时，您可以指定Docker尝试重启容器的最大次数。默认情况下，Docker将永远尝试重新启动容器。一个容器的重启次数可以通过docker inspect获得。例如，获取容器“my-container”的重启次数;

```
$ docker inspect -f "{{ .RestartCount }}" my-container
# 2
```

或者，获取容器(重新)启动的最后一次时间;

```
$ docker inspect -f "{{ .State.StartedAt }}" my-container
# 2015-03-04T23:47:07.691840179Z
```

将--restart(重启策略)与--rm(清理)标志组合会导致错误。在容器重新启动时，连接的客户端将断开连接。请参阅本页后面使用--rm(清理)标志的示例。

#### 示例

```
$ docker run --restart=always redis
```

这将运行redis容器与重启策略总是，以便如果容器退出，Docker将重启它

```
$ docker run --restart=on-failure:10 redis
```

这将运行redis容器与重启策略的失败和最大重启计数10。如果redis容器以非零退出状态连续超过10次，Docker将中止试图重新启动容器。提供最大重启限制只对故障策略有效

#### 退出状态

docker运行的退出代码提供了关于为什么容器不能运行或为什么它退出的信息。当docker运行以非零代码退出时，退出代码遵循chroot标准，如下所示:

##### 125状态  

```
$ docker run --foo busybox; echo $?

flag provided but not defined: --foo
See 'docker run --help'.
125
该退出状态表示 Docker守护进程本身的错误
```

##### 126状态

```
$ docker run busybox /etc; echo $?

docker: Error response from daemon: Container command '/etc' could not be invoked.
126
该状态表示容器启动后，要执行的默认命令无法调用
```

##### 127状态

```
$ docker run busybox foo; echo $?

docker: Error response from daemon: Container command 'foo' not found or does not exist.
127
该状态表示容器启动后，要执行的默认命令不存在
```

